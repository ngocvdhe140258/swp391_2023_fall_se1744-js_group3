USE [master]
GO
/****** Object:  Database [SWP391_Group3]    Script Date: 9/29/2023 8:10:57 AM ******/
CREATE DATABASE [SWP391_Group3]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SWP391_Group3', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\SWP391_Group3.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'SWP391_Group3_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\SWP391_Group3_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [SWP391_Group3] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SWP391_Group3].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SWP391_Group3] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SWP391_Group3] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SWP391_Group3] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SWP391_Group3] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SWP391_Group3] SET ARITHABORT OFF 
GO
ALTER DATABASE [SWP391_Group3] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SWP391_Group3] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SWP391_Group3] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SWP391_Group3] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SWP391_Group3] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SWP391_Group3] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SWP391_Group3] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SWP391_Group3] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SWP391_Group3] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SWP391_Group3] SET  ENABLE_BROKER 
GO
ALTER DATABASE [SWP391_Group3] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SWP391_Group3] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SWP391_Group3] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SWP391_Group3] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SWP391_Group3] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SWP391_Group3] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SWP391_Group3] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SWP391_Group3] SET RECOVERY FULL 
GO
ALTER DATABASE [SWP391_Group3] SET  MULTI_USER 
GO
ALTER DATABASE [SWP391_Group3] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SWP391_Group3] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SWP391_Group3] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SWP391_Group3] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [SWP391_Group3] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [SWP391_Group3] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'SWP391_Group3', N'ON'
GO
ALTER DATABASE [SWP391_Group3] SET QUERY_STORE = OFF
GO
USE [SWP391_Group3]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 9/29/2023 8:10:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[account_id] [int] IDENTITY(1,1) NOT NULL,
	[account_name] [nvarchar](255) NULL,
	[account_username] [nvarchar](255) NULL,
	[account_password] [nvarchar](50) NULL,
	[account_role] [int] default 0 NULL,
	[account_email] [varchar](255) NULL,
	[account_address] [nvarchar](255) NULL,
	[account_phone] [varchar](20) NULL,
	[account_dob] [date] NULL,
	[account_avatar] [nvarchar](50) NULL,
	[account_status] [int] default 1 NULL,
PRIMARY KEY CLUSTERED 
(
	[account_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 9/29/2023 8:10:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[category_id] [int] IDENTITY(1,1) NOT NULL,
	[category_name] [nvarchar](255) NULL,
	[category_desc] [nvarchar](1000) NULL,
PRIMARY KEY CLUSTERED 
(
	[category_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[House]    Script Date: 9/29/2023 8:10:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[House](
	[house_id] [int] IDENTITY(1,1) NOT NULL,
	[house_name] [nvarchar](255) NULL,
	[house_description] [text] NULL,
	[house_price] [decimal](10, 2) NULL,
	[house_create_date] [date] NULL,
	[house_modified_date] [date] NULL,
	[category_id] [int] NULL,
	[house_short_desc] [nvarchar](255) NULL,
	[house_create_by] [nvarchar](255) NULL,
	[house_modified_by] [nvarchar](255) NULL,
	[house_img] [nvarchar](50) NULL,
	[house_discount] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[house_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[News]    Script Date: 9/29/2023 8:10:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[News](
	[news_id] [int] IDENTITY(1,1) NOT NULL,
	[news_title] [nvarchar](255) NULL,
	[news_content] [text] NULL,
	[news_created_date] [date] NULL,
	[news_modified_date] [date] NULL,
	[news_created_by] [nvarchar](255) NULL,
	[news_modified_by] [nvarchar](255) NULL,
	[news_shortdecs] [nvarchar](255) NULL,
	[news_img] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[news_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Order_Status]    Script Date: 9/29/2023 8:10:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order_Status](
	[order_status_id] [int] IDENTITY(1,1) NOT NULL,
	[order_status_name] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[order_status_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderDetail]    Script Date: 9/29/2023 8:10:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderDetail](
	[order_detail_id] [int] IDENTITY(1,1) NOT NULL,
	[id] [int] NULL,
	[hourse_price] [decimal](10, 2) NULL,
	[house_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[order_detail_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 9/29/2023 8:10:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[order_id] [int] IDENTITY(1,1) NOT NULL,
	[account_id] [int] NULL,
	[order_date] [date] NULL,
	[order_total_price] [decimal](10, 2) NULL,
	[order_status_id] [int] NULL,
	[payment_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[order_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Payment_Method]    Script Date: 9/29/2023 8:10:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Payment_Method](
	[payment_id] [int] IDENTITY(1,1) NOT NULL,
	[payment_name] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[payment_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Terms]    Script Date: 9/29/2023 8:10:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Terms](
	[terms_id] [int] IDENTITY(1,1) NOT NULL,
	[terms_title] [nvarchar](255) NULL,
	[terms_content] [text] NULL,
	[terms_created_date] [date] NULL,
	[terms_modified_date] [date] NULL,
	[terms_created_by] [nvarchar](255) NULL,
	[terms_modified_by] [nvarchar](255) NULL,
	[terms_shortdecs] [nvarchar](255) NULL,
	[terms_img] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[terms_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Account] ON 

INSERT [dbo].[Account] ([account_id], [account_name], [account_username], [account_password], [account_role], [account_email], [account_address], [account_phone], [account_dob], [account_avatar], [account_status]) VALUES (1, N'Lan Le', N'lanle', N'123', 0, NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[Account] ([account_id], [account_name], [account_username], [account_password], [account_role], [account_email], [account_address], [account_phone], [account_dob], [account_avatar], [account_status]) VALUES (2, N'admin', N'admin', N'admin', 1, NULL, NULL, NULL, NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[Account] OFF
GO
SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([category_id], [category_name], [category_desc]) VALUES (1, N'Phòng Trọ', N'ohno')
INSERT [dbo].[Categories] ([category_id], [category_name], [category_desc]) VALUES (2, N'Nhà Cho Thuê', N'ohno')
INSERT [dbo].[Categories] ([category_id], [category_name], [category_desc]) VALUES (3, N'Căn Hộ', N'ohno')
INSERT [dbo].[Categories] ([category_id], [category_name], [category_desc]) VALUES (4, N'Mặt Bằng, Văn Phòng', N'ohno')
SET IDENTITY_INSERT [dbo].[Categories] OFF
GO
SET IDENTITY_INSERT [dbo].[House] ON 

INSERT [dbo].[House] ([house_id], [house_name], [house_description], [house_price], [house_create_date], [house_modified_date], [category_id], [house_short_desc], [house_create_by], [house_modified_by], [house_img], [house_discount]) VALUES (2, N'nam Pham', N'a normal house', CAST(999.00 AS Decimal(10, 2)), CAST(N'2003-09-09' AS Date), CAST(N'2003-09-09' AS Date), 1, N'a small house in HL', NULL, NULL, N'usercss\images\User\getaway.png', 10)
INSERT [dbo].[House] ([house_id], [house_name], [house_description], [house_price], [house_create_date], [house_modified_date], [category_id], [house_short_desc], [house_create_by], [house_modified_by], [house_img], [house_discount]) VALUES (3, N'huyen xinh gai', N'a normal house', CAST(999.00 AS Decimal(10, 2)), CAST(N'2003-09-09' AS Date), CAST(N'2003-09-09' AS Date), 1, N'a small house in HL', NULL, NULL, N'usercss\images\User\getaway.png', 10)
INSERT [dbo].[House] ([house_id], [house_name], [house_description], [house_price], [house_create_date], [house_modified_date], [category_id], [house_short_desc], [house_create_by], [house_modified_by], [house_img], [house_discount]) VALUES (4, N'Mai Hoa', N'a normal house', CAST(999.00 AS Decimal(10, 2)), CAST(N'2003-09-09' AS Date), CAST(N'2003-09-09' AS Date), 2, N'a small house in HL', NULL, NULL, N'usercss\images\User\getaway.png', 10)
INSERT [dbo].[House] ([house_id], [house_name], [house_description], [house_price], [house_create_date], [house_modified_date], [category_id], [house_short_desc], [house_create_by], [house_modified_by], [house_img], [house_discount]) VALUES (5, N'Tuan Cuong', N'a normal house', CAST(999.00 AS Decimal(10, 2)), CAST(N'2003-09-09' AS Date), CAST(N'2003-09-09' AS Date), 2, N'a small house in HL', NULL, NULL, N'usercss\images\User\getaway.png', 10)
INSERT [dbo].[House] ([house_id], [house_name], [house_description], [house_price], [house_create_date], [house_modified_date], [category_id], [house_short_desc], [house_create_by], [house_modified_by], [house_img], [house_discount]) VALUES (6, N'Tran Binh', N'a normal house', CAST(999.00 AS Decimal(10, 2)), CAST(N'2003-09-09' AS Date), CAST(N'2003-09-09' AS Date), 3, N'a small house in HL', NULL, NULL, N'usercss\images\User\getaway.png', 10)
INSERT [dbo].[House] ([house_id], [house_name], [house_description], [house_price], [house_create_date], [house_modified_date], [category_id], [house_short_desc], [house_create_by], [house_modified_by], [house_img], [house_discount]) VALUES (7, N'Do Nam', N'a normal house', CAST(999.00 AS Decimal(10, 2)), CAST(N'2003-09-09' AS Date), CAST(N'2003-09-09' AS Date), 3, N'a small house in HL', NULL, NULL, N'usercss\images\User\getaway.png', 10)
INSERT [dbo].[House] ([house_id], [house_name], [house_description], [house_price], [house_create_date], [house_modified_date], [category_id], [house_short_desc], [house_create_by], [house_modified_by], [house_img], [house_discount]) VALUES (8, N'Le huynh', N'a normal house', CAST(999.00 AS Decimal(10, 2)), CAST(N'2003-09-09' AS Date), CAST(N'2003-09-09' AS Date), 3, N'a small house in HL', NULL, NULL, N'usercss\images\User\getaway.png', 10)
INSERT [dbo].[House] ([house_id], [house_name], [house_description], [house_price], [house_create_date], [house_modified_date], [category_id], [house_short_desc], [house_create_by], [house_modified_by], [house_img], [house_discount]) VALUES (9, N'VP1', N'a normal house', CAST(999.00 AS Decimal(10, 2)), CAST(N'2003-09-09' AS Date), CAST(N'2003-09-09' AS Date), 4, N'a small house in HL', NULL, NULL, N'usercss\images\User\getaway.png', 10)
INSERT [dbo].[House] ([house_id], [house_name], [house_description], [house_price], [house_create_date], [house_modified_date], [category_id], [house_short_desc], [house_create_by], [house_modified_by], [house_img], [house_discount]) VALUES (10, N'VP2', N'a normal house', CAST(999.00 AS Decimal(10, 2)), CAST(N'2003-09-09' AS Date), CAST(N'2003-09-09' AS Date), 4, N'a small house in HL', NULL, NULL, N'usercss\images\User\getaway.png', 10)
INSERT [dbo].[House] ([house_id], [house_name], [house_description], [house_price], [house_create_date], [house_modified_date], [category_id], [house_short_desc], [house_create_by], [house_modified_by], [house_img], [house_discount]) VALUES (11, N'VP3', N'a normal house', CAST(999.00 AS Decimal(10, 2)), CAST(N'2003-09-09' AS Date), CAST(N'2003-09-09' AS Date), 4, N'a small house in HL', NULL, NULL, N'usercss\images\User\getaway.png', 10)
INSERT [dbo].[House] ([house_id], [house_name], [house_description], [house_price], [house_create_date], [house_modified_date], [category_id], [house_short_desc], [house_create_by], [house_modified_by], [house_img], [house_discount]) VALUES (12, N'VP4', N'a normal house', CAST(999.00 AS Decimal(10, 2)), CAST(N'2003-09-09' AS Date), CAST(N'2003-09-09' AS Date), 4, N'a small house in HL', NULL, NULL, N'usercss\images\User\getaway.png', 10)
SET IDENTITY_INSERT [dbo].[House] OFF
GO
ALTER TABLE [dbo].[House]  WITH CHECK ADD FOREIGN KEY([category_id])
REFERENCES [dbo].[Categories] ([category_id])
GO
ALTER TABLE [dbo].[OrderDetail]  WITH CHECK ADD FOREIGN KEY([house_id])
REFERENCES [dbo].[House] ([house_id])
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD FOREIGN KEY([account_id])
REFERENCES [dbo].[Account] ([account_id])
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD FOREIGN KEY([order_status_id])
REFERENCES [dbo].[Order_Status] ([order_status_id])
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD FOREIGN KEY([payment_id])
REFERENCES [dbo].[Payment_Method] ([payment_id])
GO
USE [master]
GO
ALTER DATABASE [SWP391_Group3] SET  READ_WRITE 
GO

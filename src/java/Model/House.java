/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import java.util.Date;

/**
 *
 * @author Admin
 */
public class House {
    private String id;
    private String name;
    private String description;
    private String price;
    private String createdDate;
    private String modifiedDate;
    Categories categories;
    private String house_short_desc;
    private String createdBy;
    private String modifiedBy;
    private String img;
    private String discount;

    public House() {
    }

    public House(String id, String name, String description, String price, String createdDate, String modifiedDate, Categories categories, String house_short_desc, String createdBy, String modifiedBy, String img, String discount) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.createdDate = createdDate;
        this.modifiedDate = modifiedDate;
        this.categories = categories;
        this.house_short_desc = house_short_desc;
        this.createdBy = createdBy;
        this.modifiedBy = modifiedBy;
        this.img = img;
        this.discount = discount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return Double.parseDouble(price);
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Categories getCategories() {
        return categories;
    }

    public void setCategories(Categories categories) {
        this.categories = categories;
    }

    public String getHouse_short_desc() {
        return house_short_desc;
    }

    public void setHouse_short_desc(String house_short_desc) {
        this.house_short_desc = house_short_desc;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public String getImg() {
        return img.split(";")[0];
    }

    public void setImg(String img) {
        this.img = img;
    }

    public int getDiscount() {
        return Integer.parseInt(discount);
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }
    
    public String getImg1() {
        return img.split(";")[0];
    }
    
    public String getImg2() {
        String[] t = img.split(";");
        return t.length>1 ? t[1] : null;
    }
    
    public String getImg3() {
        String[] t = img.split(";");
        return t.length>2 ? t[2] : null;
    }
    
    public String getImg4() {
        String[] t = img.split(";");
        return t.length>3 ? t[3] : null;
    }

    public String getImgRaw() {
        return this.img;
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

import java.sql.Date;

/**
 *
 * @author tnt
 */
public class AccountCustomer {
    private String customer_name,customer_username,customer_password,customer_email,customer_address,customer_phone,customer_avatar;
    private String customer_id;
    private Date customer_dob;
    private int status;
    private String role;
    private int roleId;
    private int status_delete;

    public AccountCustomer() {
    }

    public AccountCustomer(String customer_name, String customer_username, String customer_password, String customer_email, String customer_address, String customer_phone, String customer_avatar, String customer_id, Date customer_dob, int status, String role, int roleId, int status_delete) {
        this.customer_name = customer_name;
        this.customer_username = customer_username;
        this.customer_password = customer_password;
        this.customer_email = customer_email;
        this.customer_address = customer_address;
        this.customer_phone = customer_phone;
        this.customer_avatar = customer_avatar;
        this.customer_id = customer_id;
        this.customer_dob = customer_dob;
        this.status = status;
        this.role = role;
        this.roleId = roleId;
        this.status_delete = status_delete;
    }

    public AccountCustomer(String customer_id, String customer_name, String customer_username, String customer_password, int roleId, String customer_email, String customer_address, String customer_phone, Date customer_dob, String customer_avatar, int status) {
        this.customer_id = customer_id;
        this.customer_name = customer_name;
        this.customer_username = customer_username;
        this.customer_password = customer_password;
        this.customer_email = customer_email;
        this.customer_address = customer_address;
        this.customer_phone = customer_phone;
        this.customer_dob = customer_dob;
        this.customer_avatar = customer_avatar;
        this.status = status;
        this.roleId = roleId;
        
        setRole();
    }

    public String getRole() {
        return role;
    }

    public void setRole() {
        if(this.roleId==0) this.role = "user";
        if(this.roleId==1) this.role = "admin";
        if(this.roleId==2) this.role = "house manager";
        if(this.roleId==3) this.role = "tenant";
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public AccountCustomer(String customer_id, String customer_name, String customer_username, String customer_password, String customer_email, String customer_address, String customer_phone, Date customer_dob, String customer_avatar, String customer_status) {
        this.customer_name = customer_name;
        this.customer_username = customer_username;
        this.customer_password = customer_password;
        this.customer_email = customer_email;
        this.customer_address = customer_address;
        this.customer_phone = customer_phone;
        this.customer_avatar = customer_avatar;
        this.customer_id = customer_id;
        this.customer_dob = customer_dob;
    }
    
    

    public int getAccount_role() {
        return roleId;
    }

    public void setAccount_role(int Account_role) {
        this.roleId = Account_role;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_username() {
        return customer_username;
    }

    public void setCustomer_username(String customer_username) {
        this.customer_username = customer_username;
    }

    public String getCustomer_password() {
        return customer_password;
    }

    public void setCustomer_password(String customer_password) {
        this.customer_password = customer_password;
    }

    public String getCustomer_email() {
        return customer_email;
    }

    public void setCustomer_email(String customer_email) {
        this.customer_email = customer_email;
    }

    public String getCustomer_address() {
        return customer_address;
    }

    public void setCustomer_address(String customer_address) {
        this.customer_address = customer_address;
    }

    public String getCustomer_phone() {
        return customer_phone;
    }

    public void setCustomer_phone(String customer_phone) {
        this.customer_phone = customer_phone;
    }

    public Date getCustomer_dob() {
        return customer_dob;
    }

    public void setCustomer_dob(Date customer_dob) {
        this.customer_dob = customer_dob;
    }

    public String getCustomer_avatar() {
        return customer_avatar;
    }

    public void setCustomer_avatar(String customer_avatar) {
        this.customer_avatar = customer_avatar;
    }

    public int getStatus_delete() {
        return status_delete;
    }

    public void setStatus_delete(int status_delete) {
        this.status_delete = status_delete;
    }

    @Override
    public String toString() {
        return "AccountCustomer{" + "customer_name=" + customer_name + ", customer_username=" + customer_username + ", customer_password=" + customer_password + ", customer_email=" + customer_email + ", customer_address=" + customer_address + ", customer_phone=" + customer_phone + ", customer_avatar=" + customer_avatar + ", customer_id=" + customer_id + ", customer_dob=" + customer_dob + ", status=" + status + '}';
    }

    

    

    
   
    
    
}

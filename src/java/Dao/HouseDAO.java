/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao;

import Model.Categories;
import Model.House;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class HouseDAO extends DBContext {

    public List<House> getListHouse() {
        List<House> listHouse = new ArrayList<>();
        String sql = "select * from House";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String id = rs.getString(1);
                String name = rs.getString(2);
                String description = rs.getString(3);
                String price = rs.getString(4);
                String createdDate = rs.getString(5);
                String modifiedDate = rs.getString(6);
                Categories categories = (new CategoriesDAO()).getCateById(rs.getString(7));
                String short_desc = rs.getString(8);
                String createdBy = rs.getString(9);
                String modifiedBy = rs.getString(10);
                String image = rs.getString(11);
                String discount = rs.getString(12);
                House house = new House(id, name, description, price, createdDate, modifiedDate, categories, short_desc, createdBy, modifiedBy, image, discount);
                listHouse.add(house);
            }
        } catch (Exception e) {
            System.out.println("getListHouse: " + e.getMessage());
        }
        return listHouse;
    }

    public int getTotalHouse(String search, String[] category_id) {
        String sql = "select count(*) from House ";
        if (category_id.length > 0 && category_id != null) {
            sql += " where ";
            for (int i = 0; i < category_id.length - 1; i++) {
                sql += "category_id = " + category_id[i] + " or ";
            }
            sql += "category_id = " + category_id[category_id.length - 1];
        }
        if (!search.isEmpty()) {
            sql += "join Categories on House.category_id = Categories.category_id\n"
                    + "where House.house_name like '%" + search + "%' or Categories.category_name like '%" + search + "%'";
        }

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {

                return rs.getInt(1);
            }
        } catch (NumberFormatException | SQLException e) {
            System.out.println(e);
        }
        return 0;
    }
    
    public int getTotalHouse(String search, String[] category_id, String customer_username) {
        String sql = "select count(*) from House where house_create_by = '" + customer_username + "' ";
        if (category_id.length > 0 && category_id != null) {
            sql += " and ";
            for (int i = 0; i < category_id.length - 1; i++) {
                sql += "category_id = " + category_id[i] + " or ";
            }
            sql += "category_id = " + category_id[category_id.length - 1];
        }
        if (!search.isEmpty()) {
            sql += "join Categories on House.category_id = Categories.category_id\n"
                    + "where House.house_name like '%" + search + "%' or Categories.category_name like '%" + search + "%'";
        }

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {

                return rs.getInt(1);
            }
        } catch (NumberFormatException | SQLException e) {
            System.out.println(e);
        }
        return 0;
    }

    public List<House> getCourseByFilter(String filter, String search, int index) {
        switch (filter) {
            case "All":
                return searchHouseByAll(search, index, "");
            case "Category Name":
                return searchHouseByCateName(search, index, "");
            case "House Name":
                return searchHouseByCourseName(search, index, "");
        }
        return null;

    }

    
    public List<House> getCourseByFilter(String filter, String search, int index, String username) {
        switch (filter) {
            case "All":
                return searchHouseByAll(search, index, username);
            case "Category Name":
                return searchHouseByCateName(search, index, username);
            case "House Name":
                return searchHouseByCourseName(search, index, username);
        }
        return null;

    }
    
    private List<House> searchHouseByAll(String search, int index, String username) {
        List<House> data = new ArrayList<>();
        try {
            String strSelect = "select House.* from House join Categories\n"
                    + "on House.category_id = Categories.category_id \n"
                    + "where (Categories.category_name like '%" + search + "%'"
                    + " or House.house_name like '%" + search + "%') "
                    + " and house_create_by like '%" + username + "%'"
                    + "order by house_id desc offset ? rows fetch next 9 rows only";
            PreparedStatement st = connection.prepareStatement(strSelect);
            st.setInt(1, (index - 1) * 9);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                String id = rs.getString(1);
                String name = rs.getString(2);
                String description = rs.getString(3);
                String price = rs.getString(4);
                String createdDate = rs.getString(5);
                String modifiedDate = rs.getString(6);
                Categories categories = (new CategoriesDAO()).getCateById(rs.getString(7));
                String short_desc = rs.getString(8);
                String createdBy = rs.getString(9);
                String modifiedBy = rs.getString(10);
                String image = rs.getString(11);
                String discount = rs.getString(12);
                House house = new House(id, name, description, price, createdDate, modifiedDate, categories, short_desc, createdBy, modifiedBy, image, discount);
                data.add(house);
            }
        } catch (Exception e) {
            System.out.println("searchHouseByAll" + e.getMessage());
        }
        return data;

    }

    private List<House> searchHouseByCateName(String search, int index, String username) {
        List<House> data = new ArrayList<>();
        try {
            String strSelect = "select House.* from House join Categories\n"
                    + "on House.category_id = Categories.category_id "
                    + "where Categories.category_name like '%" + search + "%' "
                    + "and house_create_by like '%" + username + "%'"
                    + "order by house_id desc offset ? rows fetch next 9 rows only";
            PreparedStatement st = connection.prepareStatement(strSelect);
            st.setInt(1, (index - 1) * 9);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                String id = rs.getString(1);
                String name = rs.getString(2);
                String description = rs.getString(3);
                String price = rs.getString(4);
                String createdDate = rs.getString(5);
                String modifiedDate = rs.getString(6);
                Categories categories = (new CategoriesDAO()).getCateById(rs.getString(7));
                String short_desc = rs.getString(8);
                String createdBy = rs.getString(9);
                String modifiedBy = rs.getString(10);
                String image = rs.getString(11);
                String discount = rs.getString(12);
                House house = new House(id, name, description, price, createdDate, modifiedDate, categories, short_desc, createdBy, modifiedBy, image, discount);
                data.add(house);
            }
        } catch (Exception e) {
            System.out.println("searchHouseByCateName" + e.getMessage());
        }
        return data;
    }

    private List<House> searchHouseByCourseName(String search, int index, String username) {
        List<House> data = new ArrayList<>();
        try {
            String strSelect = "select * from House "
                    + "where House.house_name like '%" + search + "%' "
                     + "and house_create_by like '%" + username + "%'"
                    + "order by house_id desc offset ? rows fetch next 9 rows only";
            PreparedStatement st = connection.prepareStatement(strSelect);
            st.setInt(1, (index - 1) * 9);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                String id = rs.getString(1);
                String name = rs.getString(2);
                String description = rs.getString(3);
                String price = rs.getString(4);
                String createdDate = rs.getString(5);
                String modifiedDate = rs.getString(6);
                Categories categories = (new CategoriesDAO()).getCateById(rs.getString(7));
                String short_desc = rs.getString(8);
                String createdBy = rs.getString(9);
                String modifiedBy = rs.getString(10);
                String image = rs.getString(11);
                String discount = rs.getString(12);
                House house = new House(id, name, description, price, createdDate, modifiedDate, categories, short_desc, createdBy, modifiedBy, image, discount);
                data.add(house);
            }
        } catch (Exception e) {
            System.out.println("searchCourseByCourseName" + e.getMessage());
        }
        return data;
    }

    public List<House> listFilterHouse(int index, String sortCourse, String[] arr_category_id) {
        List<House> data = new ArrayList<>();
        try {
            String strSelect = "select * from house ";
            if (arr_category_id.length > 0 && arr_category_id != null) {
                strSelect += " where ";
                for (int i = 0; i < arr_category_id.length - 1; i++) {
                    strSelect += "category_id = " + arr_category_id[i] + " or ";
                }
                strSelect += "category_id = " + arr_category_id[arr_category_id.length - 1];
            }
            if (sortCourse.equals("0")) {
                strSelect += " order by (house_price - (house_price*house_discount/100)) desc offset ? rows fetch next 9 rows only";
            }
            if (sortCourse.equals("1")) {
                strSelect += " order by (house_price - (house_price*house_discount/100)) asc offset ? rows fetch next 9 rows only";
            }
            PreparedStatement st = connection.prepareStatement(strSelect);
            st.setInt(1, (index - 1) * 9);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                String id = rs.getString(1);
                String name = rs.getString(2);
                String description = rs.getString(3);
                String price = rs.getString(4);
                String createdDate = rs.getString(5);
                String modifiedDate = rs.getString(6);
                Categories categories = (new CategoriesDAO()).getCateById(rs.getString(7));
                String short_desc = rs.getString(8);
                String createdBy = rs.getString(9);
                String modifiedBy = rs.getString(10);
                String image = rs.getString(11);
                String discount = rs.getString(12);
                House house = new House(id, name, description, price, createdDate, modifiedDate, categories, short_desc, createdBy, modifiedBy, image, discount);
                data.add(house);
            }
        } catch (Exception e) {
            System.out.println("listFilterHouse: " + e.getMessage());
        }
        return data;
    }
    
    public List<House> listFilterHouse(int index, String sortCourse, String[] arr_category_id, String customer_username) {
        List<House> data = new ArrayList<>();
        try {
            String strSelect = "select * from house where house_create_by = '" + customer_username + "'";
            if (arr_category_id.length > 0 && arr_category_id != null) {
                strSelect += " and (";
                for (int i = 0; i < arr_category_id.length - 1; i++) {
                    strSelect += "category_id = " + arr_category_id[i] + " or ";
                }
                strSelect += "category_id = " + arr_category_id[arr_category_id.length - 1]+")";
            }
            if (sortCourse.equals("0")) {
                strSelect += " order by (house_price - (house_price*house_discount/100)) desc offset ? rows fetch next 9 rows only";
            }
            if (sortCourse.equals("1")) {
                strSelect += " order by (house_price - (house_price*house_discount/100)) asc offset ? rows fetch next 9 rows only";
            }
            PreparedStatement st = connection.prepareStatement(strSelect);
            st.setInt(1, (index - 1) * 9);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                String id = rs.getString(1);
                String name = rs.getString(2);
                String description = rs.getString(3);
                String price = rs.getString(4);
                String createdDate = rs.getString(5);
                String modifiedDate = rs.getString(6);
                Categories categories = (new CategoriesDAO()).getCateById(rs.getString(7));
                String short_desc = rs.getString(8);
                String createdBy = rs.getString(9);
                String modifiedBy = rs.getString(10);
                String image = rs.getString(11);
                String discount = rs.getString(12);
                House house = new House(id, name, description, price, createdDate, modifiedDate, categories, short_desc, createdBy, modifiedBy, image, discount);
                data.add(house);
            }
        } catch (Exception e) {
            System.out.println("listFilterHouse: " + e.getMessage());
        }
        return data;
    }

    public List<House> listHouseOfCateByPaging(int index) {
        List<House> data = new ArrayList<>();
        try {
            String strSelect = "select * from House order by house_id desc\n"
                    + "offset ? rows fetch next 9 rows only";

            PreparedStatement st = connection.prepareStatement(strSelect);

            st.setInt(1, (index - 1) * 9);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                String id = rs.getString(1);
                String name = rs.getString(2);
                String description = rs.getString(3);
                String price = rs.getString(4);
                String createdDate = rs.getString(5);
                String modifiedDate = rs.getString(6);
                Categories categories = (new CategoriesDAO()).getCateById(rs.getString(7));
                String short_desc = rs.getString(8);
                String createdBy = rs.getString(9);
                String modifiedBy = rs.getString(10);
                String image = rs.getString(11);
                String discount = rs.getString(12);
                House house = new House(id, name, description, price, createdDate, modifiedDate, categories, short_desc, createdBy, modifiedBy, image, discount);
                data.add(house);
            }
        } catch (Exception e) {
            System.out.println("listHouseOfCateByPaging" + e.getMessage());
        }
        return data;
    }
    
    public List<House> listHouseOfCateByPaging(int index, String customer_username) {
        List<House> data = new ArrayList<>();
        try {
            String strSelect = "select * from House where house_create_by = ? order by house_id desc\n"
                    + "offset ? rows fetch next 9 rows only";

            PreparedStatement st = connection.prepareStatement(strSelect);
            st.setString(1, customer_username);
            st.setInt(2, (index - 1) * 9);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                String id = rs.getString(1);
                String name = rs.getString(2);
                String description = rs.getString(3);
                String price = rs.getString(4);
                String createdDate = rs.getString(5);
                String modifiedDate = rs.getString(6);
                Categories categories = (new CategoriesDAO()).getCateById(rs.getString(7));
                String short_desc = rs.getString(8);
                String createdBy = rs.getString(9);
                String modifiedBy = rs.getString(10);
                String image = rs.getString(11);
                String discount = rs.getString(12);
                House house = new House(id, name, description, price, createdDate, modifiedDate, categories, short_desc, createdBy, modifiedBy, image, discount);
                data.add(house);
            }
        } catch (Exception e) {
            System.out.println("listHouseOfCateByPaging" + e.getMessage());
        }
        return data;
    }

    public void deleteHouse(String id) {
        String sql = "delete from House where house_id = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, id);
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println("deleteHouse: " + e.getMessage());
        }
    }

    public House getHouseById(String id) {
        String sql = "select * from House where house_id = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String name = rs.getString(2);
                String description = rs.getString(3);
                String price = rs.getString(4);
                String createdDate = rs.getString(5);
                String modifiedDate = rs.getString(6);
                Categories categories = (new CategoriesDAO()).getCateById(rs.getString(7));
                String short_desc = rs.getString(8);
                String createdBy = rs.getString(9);
                String modifiedBy = rs.getString(10);
                String image = rs.getString(11);
                String discount = rs.getString(12);
                return new House(id, name, description, price, createdDate, modifiedDate, categories, short_desc, createdBy, modifiedBy, image, discount);
            }
        } catch (Exception e) {
            System.out.println("getHouseById: " + e.getMessage());
        }
        return new House();
    }

    public void addNewHouse(House house) {
        String sql = "INSERT INTO [dbo].[House]\n"
                + "           ([house_name]\n"
                + "           ,[house_description]\n"
                + "           ,[house_price]\n"
                + "           ,[house_create_date]\n"
                + "           ,[house_modified_date]\n"
                + "           ,[category_id]\n"
                + "           ,[house_short_desc]\n"
                + "           ,[house_create_by]\n"
                + "           ,[house_modified_by]\n"
                + "           ,[house_img]\n"
                + "           ,[house_discount])\n"
                + "     VALUES (?,?,?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, house.getName());
            ps.setString(2, house.getDescription());
            ps.setString(3, house.getPrice() + "");
            ps.setString(4, house.getCreatedDate());
            ps.setString(5, house.getModifiedDate());
            ps.setString(6, house.getCategories().getCategory_id() + "");
            ps.setString(7, house.getHouse_short_desc());
            ps.setString(8, house.getCreatedBy());
            ps.setString(9, house.getModifiedBy());
            ps.setString(10, house.getImgRaw());
            ps.setString(11, house.getDiscount() + "");
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println("addNewHouse: " + e.getMessage());
        }

    }

    public void updateHouse(House house) {
        String sql = "UPDATE [dbo].[House]\n"
                + "   SET [house_name] = ?\n"
                + "      ,[house_description] = ?\n"
                + "      ,[house_price] = ?\n"
                + "      ,[house_create_date] = ?\n"
                + "      ,[house_modified_date] = ?\n"
                + "      ,[category_id] = ?\n"
                + "      ,[house_short_desc] = ?\n"
                + "      ,[house_create_by] = ?\n"
                + "      ,[house_modified_by] = ?\n"
                + "      ,[house_img] = ?\n"
                + "      ,[house_discount] = ?\n"
                + " WHERE house_id = ?";
        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setString(1, house.getName());
            ps.setString(2, house.getDescription());
            ps.setString(3, house.getPrice() + "");
            ps.setString(4, house.getCreatedDate());
            ps.setString(5, house.getModifiedDate());
            ps.setString(6, house.getCategories().getCategory_id() + "");
            ps.setString(7, house.getHouse_short_desc());
            ps.setString(8, house.getCreatedBy());
            ps.setString(9, house.getModifiedBy());
            ps.setString(10, house.getImgRaw());
            ps.setString(11, house.getDiscount() + "");
            ps.setString(12, house.getId() + "");
            ps.executeUpdate();
        } catch (Exception e) {
            System.out.println("updateHouse: " + e.getMessage());
        }
    }
}

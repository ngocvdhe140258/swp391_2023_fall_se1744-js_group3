/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao;

import Model.AccountAdmin;
import Model.AccountCustomer;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class AccountDAO extends DBContext {

    public AccountCustomer login(String username, String password) {
        String sql = "select * from Account where account_username=? and account_password=?";
        try {

            PreparedStatement st = connection.prepareStatement(sql);

            st.setString(1, username);
            st.setString(2, password);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new AccountCustomer(rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getDate(9),
                        rs.getString(10),
                        rs.getInt(11));
            }
        } catch (Exception e) {
            System.out.println("login: " + e.getMessage());
        }

        return null;
    }

    public boolean checkStatus(String username, String password) {
        String sql = "select * from Account where account_username=? and account_password =? and accountdelete_status = 0";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, username);
            st.setString(2, password);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    public boolean checkActive(String username, String password) {
        String sql = "select * from Account where account_username=? and account_password =? and account_status = 0";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, username);
            st.setString(2, password);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return true;
            }
        } catch (Exception e) {
        }
        return false;
    }

    public AccountCustomer checkAccountExist(String username) {
        String sql = "select * from Account where account_username = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, username);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new AccountCustomer(rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getDate(9),
                        rs.getString(10),
                        rs.getInt(11));

            }
        } catch (Exception e) {
        }
        return null;
    }

    public AccountCustomer checkEmailExist(String email) {
        String sql = "select * from Account where account_email = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new AccountCustomer(rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getDate(9),
                        rs.getString(10),
                        rs.getInt(11));
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public void signup(String customer_name,
            String username,
            String password,
            String email,
            String customer_address,
            String customer_phone,
            String customer_dob,
            String account_status,
            String role,
            String status_delete) {
        String sql = "INSERT INTO [dbo].[Account]\n"
                + "([account_name]\n"
                + ",[account_username]\n"
                + ",[account_password]\n"
                + ",[account_email]\n"
                + ",[account_address]\n"
                + ",[account_phone]\n"
                + ",[account_dob]\n"
                + ",[account_status]\n"
                + ",[account_role]\n"
                + ",[accountdelete_status])\n"
                + "VALUES\n"
                + "                (?, ?, ?, ?, ?, ?, ?, ?, ?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, customer_name);
            st.setString(2, username);
            st.setString(3, password);
            st.setString(4, email);
            st.setString(5, customer_address);
            st.setString(6, customer_phone);
            st.setString(7, customer_dob);
            st.setString(8, account_status);
            st.setString(9, role);
            st.setString(10, status_delete);
            st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void newPassword(String newPass, String email) {
        String sql = "update Account set account_password = ? where account_email=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, newPass);
            st.setString(2, email);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public boolean updateStatus(int status, String id) {
        String sql = "update Account set accountdelete_status = ? where account_id=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, status);
            st.setString(2, id);
            st.executeUpdate();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void changePass(String newPass, String username) {
        String sql = "update Account set account_password = ? where account_username=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, newPass);
            st.setString(2, username);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public AccountCustomer getProfileByID(String customer_id) {
        String sql = "select * from Account where account_id = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, customer_id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                AccountCustomer ac = new AccountCustomer(rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getDate(9),
                        rs.getString(10),
                        rs.getInt(11));
                return ac;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public List<AccountCustomer> getAll() {
        String sql = "select * from Account where accountdelete_status = 0 and (account_role = 2 or account_role = 3)";
        List<AccountCustomer> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                AccountCustomer ac = new AccountCustomer(rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getDate(9),
                        rs.getString(10),
                        rs.getInt(11));
                list.add(ac);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public void editProfile(String customer_id, String customer_name, String customer_email, String customer_address, String customer_phone, String customer_dob) {
        String sql = "update Account_Customer set [account_name] = ?, [account_email] = ?, [account_address]=?,[account_phone]=?,[account_dob]=? where [account_id]=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, customer_name);
            st.setString(2, customer_email);
            st.setString(3, customer_address);
            st.setString(4, customer_phone);
            st.setString(5, customer_dob);
            st.setString(6, customer_id);
            st.executeUpdate();
        } catch (Exception e) {
        }
    }

    public AccountCustomer selectAccountByUsername(String username) {
        String sql = "select * from Account where account_username=?";
        try {

            PreparedStatement st = connection.prepareStatement(sql);

            st.setString(1, username);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new AccountCustomer(rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getInt(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getDate(9),
                        rs.getString(10),
                        rs.getInt(11));
            }
        } catch (Exception e) {
            System.out.println("login: " + e.getMessage());
        }
        return null;
    }

}

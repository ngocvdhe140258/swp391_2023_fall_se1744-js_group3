/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Dao;


import Model.Customer;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class CustomerDao extends DBContext{
    
    public List<Customer> listAllAccountCustomer() {
        List<Customer> data = new ArrayList<>();
        try {
            String strSelect = "select * from  Account where account_role = 3";
            PreparedStatement st = connection.prepareStatement(strSelect);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                String cusID = rs.getString(1);
                String cusName = rs.getString(2);
                String cusUserName = rs.getString(3);
                String cusPassword = rs.getString(4);
                String role = rs.getString(5);
                String cusEmail = rs.getString(6);
                String cusAddress = rs.getString(7);
                String phone = rs.getString(8);
                String cusDob = rs.getString(9);
                String cusAvatar = rs.getString(10);
                String cusStatus = rs.getString(11);
               
                data.add(new Customer(cusID, cusName, cusUserName, cusPassword, role, cusEmail, cusAddress, phone, cusDob, cusAvatar, cusStatus));
            }
        } catch (Exception e) {
            System.out.println("listAllAccount" + e.getMessage());
        }
        return data;
    }
    public List<Customer> listAllAccountCustomerByPaging(int index) {
        List<Customer> data = new ArrayList<>();
        try {
            String strSelect = "select * from Account  where account_role = 3\n" +
"                   order by account_id asc\n" +
"                    offset ? rows fetch next 10 rows only";
            PreparedStatement st = connection.prepareStatement(strSelect);
            st.setInt(1, (index - 1) * 10);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                String cusID = rs.getString(1);
                String cusName = rs.getString(2);
                String cusUserName = rs.getString(3);
                String cusPassword = rs.getString(4);
                String role = rs.getString(5);
                String cusEmail = rs.getString(6);
                String cusAddress = rs.getString(7);
                String phone = rs.getString(8);
                String cusDob = rs.getString(9);
                String cusAvatar = rs.getString(10);
                String cusStatus = rs.getString(11);      
                data.add(new Customer(cusID, cusName, cusUserName, cusPassword, role, cusEmail, cusAddress, phone, cusDob, cusAvatar, cusStatus));
            }
        } catch (Exception e) {
            System.out.println("listAllAccount" + e.getMessage());
        }
        return data;
    }
    
    public List<Customer> searchAccountCustomerByUserName(String search,int index) {
        List<Customer> data = new ArrayList<>();
        try {
            String strSelect = "select * from Account  where account_role = 3 and account_username like '%" + search + "%'\n" +
"                   order by account_id asc\n" +
"                    offset ? rows fetch next 10 rows only";
            PreparedStatement st = connection.prepareStatement(strSelect);
            st.setInt(1, (index - 1) * 10);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                String cusID = rs.getString(1);
                String cusName = rs.getString(2);
                String cusUserName = rs.getString(3);
                String cusPassword = rs.getString(4);
                String role = rs.getString(5);
                String cusEmail = rs.getString(6);
                String cusAddress = rs.getString(7);
                String phone = rs.getString(8);
                String cusDob = rs.getString(9);
                String cusAvatar = rs.getString(10);
                String cusStatus = rs.getString(11);      
                data.add(new Customer(cusID, cusName, cusUserName, cusPassword, role, cusEmail, cusAddress, phone, cusDob, cusAvatar, cusStatus));
            }
        } catch (Exception e) {
            System.out.println("listAllAccount" + e.getMessage());
        }
        return data;
    }
    
    public int getTotalAccount(String search) {
        String sql = "select count(*) from Account where account_role = 3";
        if (!search.isEmpty()) {
            sql += "and account_username like '%" + search + "%'";
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {

                return rs.getInt(1);
            }
        } catch (NumberFormatException | SQLException e) {
            System.out.println(e);
        }
        return 0;
    }
    
     public void changeStatus(String customer_id, String status) {
         if(status.equals("Active")){
             try {
            String strSelect = "update Account set account_status = 1 where account_id = ? ";
            PreparedStatement st = connection.prepareStatement(strSelect);
            st.setString(1, customer_id);
            st.execute();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
         }else{
             try {
            String strSelect = "update Account set account_status = 0 where account_id = ? ";
            PreparedStatement st = connection.prepareStatement(strSelect);
            st.setString(1, customer_id);
            st.execute();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
         }
        
    }
    
     
    
     
     public static void main(String[] args) {
         CustomerDao cus = new CustomerDao();
//         cus.changeStatus("3");
    }
}
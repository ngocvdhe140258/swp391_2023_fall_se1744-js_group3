package Controller;

import Dao.AccountDAO;
import Model.AccountCustomer;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;

/**
 *
 * @author anhdu
 */
@WebServlet(name = "AdminUserList", urlPatterns = {"/AdminUserList"})
public class AdminUserList extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");

        if (action == null || action.isEmpty()) {
            List<AccountCustomer> userList = new AccountDAO().getAll();

            request.setAttribute("userList", userList);
            request.getRequestDispatcher("AdminUserList.jsp").forward(request, response);
            return;
        }

        if (action.equals("details")) {
            AccountCustomer user = new AccountDAO().getProfileByID(request.getParameter("id"));

            request.setAttribute("user", user);
            request.getRequestDispatcher("AdminUserDetails.jsp").forward(request, response);
            return;
        }

        if (action.equals("delete")) {

            String msg;
            String id = request.getParameter("id");

            boolean res = new AccountDAO().updateStatus(1, id);
            if (res) {
                msg = "Delete success";
            } else {
                msg = "Delete failed!";
            }

            List<AccountCustomer> userList = new AccountDAO().getAll();

            request.setAttribute("userList", userList);
            request.setAttribute("mess", msg);
            request.getRequestDispatcher("AdminUserList.jsp").forward(request, response);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = request.getParameter("action");

        if (action.equals("update")) {
            String id = request.getParameter("id");
            String name = request.getParameter("name");
            String email = request.getParameter("email");
            String address = request.getParameter("address");
            String phone = request.getParameter("phone");
            
            new AccountDAO().editProfile(id, name, email, address, phone, null);
            
            response.sendRedirect("AdminUserList?action=details&id=" + id + "&success");
            return;
        }

    }
}

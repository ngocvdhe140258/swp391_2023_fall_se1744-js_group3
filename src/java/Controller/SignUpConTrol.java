/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import Dao.AccountDAO;
import Model.AccountCustomer;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Random;
import validate.Validate;

/**
 *
 * @author mih
 */
public class SignUpConTrol extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String customer_name = request.getParameter("customer_name");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String repassword = request.getParameter("repassword");
        String email = request.getParameter("email");
        String customer_phone = request.getParameter("customer_phone");
        String customer_address = request.getParameter("customer_address");
        String customer_dob = request.getParameter("customer_dob");
        String role = request.getParameter("role");
        
        request.setAttribute("name", customer_name);
        request.setAttribute("username", username);
        request.setAttribute("password", password);
        request.setAttribute("repassword", repassword);
        request.setAttribute("email", email);
        request.setAttribute("phone", customer_phone);
        request.setAttribute("address", customer_address);
        request.setAttribute("dob", customer_dob);
        request.setAttribute("role", role);
        
        String errorMsg;
        
//        if (!Validate.isValidUsername(username)) errorMsg = "Invalid username";
//        if (!Validate.isValidPhone(customer_phone)) errorMsg = "Invalid phone";
//        if (!Validate.isValidEmail(email)) errorMsg = "Invalid email";
//        if (!Validate.isValidUsername(username)) errorMsg = "Invalid username";
        
        if (Validate.isValidUsername(username)
                && Validate.isValidPassword(password)
                && Validate.isValidEmail(email)
                && Validate.isValidPhone(customer_phone)) {
            //if password != repassword -> login.jsp
            if (!password.equals(repassword)) {
                request.setAttribute("smess", "Wrong repassword");
                request.getRequestDispatcher("signup.jsp").forward(request, response);
            } else {
                AccountDAO dao = new AccountDAO();
                
                //check duplicate email
                AccountCustomer a2 = dao.checkEmailExist(email);
                if(a2 != null)  {
                    request.setAttribute("smess", "Email already exist");
                    request.getRequestDispatcher("signup.jsp").forward(request, response);
                    return;
                }
                
                //check duplicate username
                AccountCustomer a = dao.checkAccountExist(username);
                if (a == null) {
                    //Can sign up
                    HttpSession session = request.getSession();
                    session.setAttribute("name", customer_name);
                    session.setAttribute("username", username);
                    session.setAttribute("password", password);
                    session.setAttribute("email", email);
                    session.setAttribute("address", customer_address);
                    session.setAttribute("phone", customer_phone);
                    session.setAttribute("role", role);
                    request.getSession().setAttribute("dob", changeDateFormat(customer_dob));
                    
                    String otp = generateOtp();
                    request.getSession().setAttribute("code", otp);
                    
                    request.setAttribute("link", "confirmSignup");
                    request.setAttribute("username", customer_name);
                    request.setAttribute("otp", otp);
                    request.setAttribute("email", email);
                    request.getRequestDispatcher("confirmOtp.jsp").forward(request, response);
                    
                } else {
                    //back to login.jsp
                    request.setAttribute("smess", "User Name already exist");
                    request.getRequestDispatcher("signup.jsp").forward(request, response);
                }
            }
            //sign up
        } else {
            if (!Validate.isValidUsername(username)) {
                request.setAttribute("smess", "Username must start with a letter and are 6 to 30 characters long, consisting of letters, digits, or underscores.");
                request.getRequestDispatcher("signup.jsp").forward(request, response);
            }
            if (!password.equals(repassword)) {
                request.setAttribute("smess", "2 Password not match");
                request.getRequestDispatcher("signup.jsp").forward(request, response);
            }
            if (!Validate.isValidPassword(password)) {
                request.setAttribute("smess", "Password contains at least 8 characters and at most 20 characters, at least one digit, one upper case alphabet, at least one special character which includes !@#$%&*()-+=^.");
                request.getRequestDispatcher("signup.jsp").forward(request, response);
            }
            if(!Validate.isValidEmail(email)){
                request.setAttribute("smess", "Email is invalid");
                request.getRequestDispatcher("signup.jsp").forward(request, response);
            }
            if(!Validate.isValidPhone(customer_phone)){
                request.setAttribute("smess", "Phone is invalid");
                request.getRequestDispatcher("signup.jsp").forward(request, response);
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }
    
    private String changeDateFormat(String s) {
        String[] ss = s.split("-");
        return ss[0] + "-" + ss[1] + "-" + ss[2];
    }
    
    public String generateOtp() {
        Random random = new Random();
        int randomNumber = random.nextInt(900000) + 100000;
        return String.valueOf(randomNumber);
    }
    public static void main(String[] args) {
        AccountDAO dao = new AccountDAO();
        AccountCustomer a2 = dao.checkEmailExist("sonnl06062001@gmail.com");
        AccountCustomer a3 = dao.checkAccountExist("hongha");
        System.out.println(a3);
    }

}

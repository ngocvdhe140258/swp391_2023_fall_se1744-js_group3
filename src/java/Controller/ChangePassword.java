/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import Dao.AccountDAO;
import Model.AccountCustomer;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import validate.Validate;

/**
 *
 * @author acer
 */
@WebServlet(name = "ChangePassword", urlPatterns = {"/change"})
public class ChangePassword extends HttpServlet {


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ChangePassword</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ChangePassword at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String customer_username = request.getParameter("customer_username");
        String oldpassword = request.getParameter("oldpassword");
        String password = request.getParameter("password");
        String confirmpassword = request.getParameter("confirmPassword");
        AccountDAO dao = new AccountDAO();
        AccountCustomer a = dao.login(customer_username, oldpassword);
        if (a == null) {
            //Old password wrong
            request.setAttribute("mess", "Wrong old password");
            request.getRequestDispatcher("change.jsp").forward(request, response);
        } else {
            if (Validate.isValidPassword(password)) {
                if (!password.equals(confirmpassword)) {
                    request.setAttribute("mess", "Wrong repassword");
                    request.getRequestDispatcher("change.jsp").forward(request, response);
                }else{
                    AccountCustomer ac = new AccountCustomer();
                    dao.changePass(password, customer_username);
                    
                    request.setAttribute("mess", "Password changed");
                    request.getRequestDispatcher("change.jsp").forward(request, response);
                }
            }else {
                request.setAttribute("mess", "Password contains at least 8 characters and at most 20 characters, at least one digit, one upper case alphabet, at least one special character which includes !@#$%&*()-+=^.");
                request.getRequestDispatcher("change.jsp").forward(request, response);
            }
        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

package Controller;

import Dao.AccountDAO;
import Model.AccountCustomer;
import java.io.IOException;
import java.util.Properties;
import java.util.Random;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 * Servlet implementation class ForgotPassword
 */
@WebServlet("/forgotPassword")
public class ForgotPassword extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String email = request.getParameter("email").trim();
        
        AccountDAO dao = new AccountDAO();
        AccountCustomer a = dao.checkEmailExist(email);
        
        if (a == null) {
            request.setAttribute("mess", "email does not match any account");
            request.getRequestDispatcher("forgotPassword.jsp").forward(request, response);
        } else {
            if (email!=null && !email.equals("")) {
                
                String otp = generateOtp();
                request.getSession().setAttribute("code", otp);
                request.getSession().setAttribute("forgotAcc", a);

                request.setAttribute("link", "newPassword");
                request.setAttribute("username", a.getCustomer_name());
                request.setAttribute("otp", otp);
                request.setAttribute("email", email);
                request.getRequestDispatcher("confirmOtp.jsp").forward(request, response);
                
            } else {
                request.setAttribute("mess", "email cant be empty");
                request.getRequestDispatcher("forgotPassword.jsp").forward(request, response);
            }
        }
    }
    
    public String generateOtp() {
        Random random = new Random();
        int randomNumber = random.nextInt(900000) + 100000;
        return String.valueOf(randomNumber);
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("login.jsp");
    }

}

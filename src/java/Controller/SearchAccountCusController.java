/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import Dao.CustomerDao;
import Model.Customer;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author Admin
 */
public class SearchAccountCusController extends HttpServlet{

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/OverriddenMethodBody
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        CustomerDao cd = new CustomerDao();      
        String search = req.getParameter("search");
        
        String index_row = req.getParameter("index");
        if(index_row==null){
            index_row="1";
        }
        int index = Integer.parseInt(index_row);
        
        int count = cd.getTotalAccount(search);
        int endPage = count/10;
        if(count%10!=0){
            endPage++;
        }       
        List<Customer> list = cd.searchAccountCustomerByUserName(search,index);
        req.setAttribute("mode", "searchcus");
        req.setAttribute("endP", endPage);
        req.setAttribute("index", index);      
        req.setAttribute("search", search);
        req.setAttribute("list", list);
        req.getRequestDispatcher("accountcustomer.jsp").forward(req, resp);
        
    }
    public static void main(String[] args) {
        CustomerDao cd = new CustomerDao();      
        List<Customer> list = cd.searchAccountCustomerByUserName("thanh",1);
        System.out.println(list);
    }
    
    
}
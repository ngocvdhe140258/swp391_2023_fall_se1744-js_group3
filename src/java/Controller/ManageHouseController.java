/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package Controller;

import Dao.CategoriesDAO;
import Dao.HouseDAO;
import Model.AccountCustomer;
import Model.Categories;
import Model.House;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;

/**
 *
 * @author Admin
 */
@WebServlet(name="ManageHouseController", urlPatterns={"/managehouse"})
public class ManageHouseController extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet QLHouseController</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet QLHouseController at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        HouseDAO cd = new HouseDAO();
        CategoriesDAO cated = new CategoriesDAO();
        //get page
        String index_row = request.getParameter("index");
        //if not change page --> assign to 1st page
        if(index_row==null){
            index_row="1";
        }
        int index = Integer.parseInt(index_row);
        //get total house in list
        int count = cd.getTotalHouse("",new String[0], ((AccountCustomer)(request.getSession().getAttribute("acc"))).getCustomer_username());
        //get the last page
        int endPage = count/9;
        if(count%9!=0){
            endPage++;
        }
        //get list hosue by page number
        List<House> listHouseOfCateByPaging = cd.listHouseOfCateByPaging(index, ((AccountCustomer)(request.getSession().getAttribute("acc"))).getCustomer_username());
        List<Categories> listCategories = cated.getListCategories();
        request.setAttribute("index", index);
        request.setAttribute("endP", endPage);
        request.setAttribute("mode", "houseofcategory");
        request.setAttribute("listCategories", listCategories);
        request.setAttribute("listHouseOfCateByPaging", listHouseOfCateByPaging);
        request.getRequestDispatcher("ManageHouse.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

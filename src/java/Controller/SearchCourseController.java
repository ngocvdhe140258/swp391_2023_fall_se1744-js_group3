/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import Dao.CategoriesDAO;
import Dao.CourseDAO;
import Dao.HouseDAO;
import Model.AccountCustomer;
import Model.Categories;
import Model.Course;
import Model.House;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Servlet responsible for handling search-related requests for courses.
 */
public class SearchCourseController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // This method could be used to handle POST requests, but it's currently empty.
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // Create instances of data access objects (DAOs) for houses and categories.
        HouseDAO cd = new HouseDAO();
        CategoriesDAO cated = new CategoriesDAO();

        // Get filter and search parameters from the HTTP request.
        String filtersearch = req.getParameter("filtersearch");
        String searchContent = req.getParameter("search");

        // Retrieve a list of all categories from the database.
        List<Categories> listCategories = cated.getListCategories();

        // Get the index of the current page.
        String index_row = req.getParameter("index");
        if (index_row == null) {
            index_row = "1";
        }
        int index = Integer.parseInt(index_row);

        // Get the total count of houses based on the search content and no specific category filter.
        int count = cd.getTotalHouse(searchContent, new String[0]);

        // Calculate the number of pages needed for pagination.
        int endPage = count / 9;
        if (count % 9 != 0) {
            endPage++;
        }
        String username = ((AccountCustomer)(req.getSession().getAttribute("acc"))).getCustomer_username();

        // Retrieve a list of houses based on search criteria and pagination.
        List<House> listSearchHouse = cd.getCourseByFilter(filtersearch, searchContent, index, username);

        // Set attributes in the request for rendering in the view.
        req.setAttribute("endP", endPage);

        req.setAttribute("filtersearch", filtersearch);
        req.setAttribute("search", searchContent);
        req.setAttribute("index", index);
        req.setAttribute("listCategories", listCategories);

        if (req.getParameter("modeManage") != null) {
            req.setAttribute("mode", "houseofcategory");
            req.setAttribute("listHouseOfCateByPaging", listSearchHouse);
            req.getRequestDispatcher("ManageHouse.jsp").forward(req, resp);
        } // Forward the request to the "courseofcategory.jsp" view for rendering.
        else {
            req.setAttribute("listCourseOfCateByPaging", listSearchHouse);
            req.setAttribute("mode", "searchcourse");
            req.getRequestDispatcher("courseofcategory.jsp").forward(req, resp);
        }
    }
}

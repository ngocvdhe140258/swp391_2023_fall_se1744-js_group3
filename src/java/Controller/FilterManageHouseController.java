/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package Controller;

import Dao.CategoriesDAO;
import Dao.HouseDAO;
import Model.AccountCustomer;
import Model.Categories;
import Model.House;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Admin
 */
@WebServlet(name="FilterManageHouseController", urlPatterns={"/filtermanagehouse"})
public class FilterManageHouseController extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet FilterQLHouseController</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet FilterQLHouseController at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        HouseDAO cd = new HouseDAO();
        CategoriesDAO cated = new CategoriesDAO();
        List<Categories> listCategories = cated.getListCategories();
        String sortHouse = request.getParameter("sortHouse");
        if(sortHouse == null) sortHouse = "0";
        String[] category_id = request.getParameterValues("category_id");
        List<String> checkedCategories = new ArrayList<>();
        if (category_id != null) {
            checkedCategories.addAll(Arrays.asList(category_id));
        }

        HttpSession session = request.getSession();
        session.setAttribute("sortHouse", sortHouse);
        String index_row = request.getParameter("index");
        if (index_row == null) {
            index_row = "1";
        }
        int index = Integer.parseInt(index_row);
        int count;
        List<House> listFilterHouse;
        if (category_id != null && category_id.length > 0) {
            count = cd.getTotalHouse("", category_id, ((AccountCustomer)(request.getSession().getAttribute("acc"))).getCustomer_username());
            listFilterHouse = cd.listFilterHouse(index, sortHouse, category_id, ((AccountCustomer)(request.getSession().getAttribute("acc"))).getCustomer_username());
            
        } else {
            count = cd.getTotalHouse("", new String[0]);
            listFilterHouse = cd.listFilterHouse(index, sortHouse, new String[0], ((AccountCustomer)(request.getSession().getAttribute("acc"))).getCustomer_username());
        }
        
        
        int endPage = count / 9;
        if (count % 9 != 0) {
            endPage++;
        }
        String selectedCategories_row = String.join(",", checkedCategories);
        String[] selectedCategories_arr = selectedCategories_row.split(",");
        String selectedCategories = "";
        if (!selectedCategories_row.isEmpty()) {
            for (int i = 0; i < selectedCategories_arr.length; i++) {
                selectedCategories += "&category_id=" + selectedCategories_arr[i];
            }
        }
        request.setAttribute("selectedCategories", selectedCategories);
        request.setAttribute("index", index);
        request.setAttribute("endP", endPage);
        request.setAttribute("mode", "filter");
        request.setAttribute("listCategories", listCategories);
        request.setAttribute("category_id", category_id);
        request.setAttribute("checkedCategories", checkedCategories);
        request.setAttribute("listHouseOfCateByPaging", listFilterHouse);
        request.getRequestDispatcher("ManageHouse.jsp").forward(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

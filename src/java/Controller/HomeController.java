/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import Dao.CategoriesDAO;
import Dao.CourseDAO;
import Dao.HouseDAO;
import Model.Categories;
import Model.Course;
import Model.House;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Servlet responsible for handling requests related to the home page.
 */
public class HomeController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // Create a HouseDAO instance to interact with the database.
        HouseDAO houseDAO = new HouseDAO();
        
        // Retrieve a list of houses from the database.
        List<House> listHouse = houseDAO.getListHouse();
        
        // Set the list of houses as an attribute in the request.
        req.setAttribute("listHouse", listHouse);
        
        // Forward the request to the "home.jsp" view for rendering.
        req.getRequestDispatcher("home.jsp").forward(req, resp);
    }
}

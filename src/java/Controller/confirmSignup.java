package Controller;

import Dao.AccountDAO;
import Model.AccountCustomer;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet(name = "ConfirmSignup", urlPatterns = {"/confirmSignup"})
public class confirmSignup extends HttpServlet {


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String otp = request.getParameter("otp");
        String code = (String)request.getSession().getAttribute("code");
        
        if(otp.equals(code)) {
            HttpSession session = request.getSession();
            String name = (String) session.getAttribute("name");
            String username = (String) session.getAttribute("username");
            String password = (String) session.getAttribute("password");
            String email = (String) session.getAttribute("email");
            String address = (String) session.getAttribute("address");
            String phone = (String) session.getAttribute("phone");
            String dob = (String) session.getAttribute("dob");
            String role = (String) session.getAttribute("role");
            
            new AccountDAO().signup(name, username, password, email, address, phone, dob, "0",role,"0");
            request.setAttribute("mess", "Sign Up successful. Login Now");
            request.getRequestDispatcher("login.jsp").forward(request, response);
        } else {
            request.setAttribute("smess", "Wrong otp!");
            request.getRequestDispatcher("signup.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

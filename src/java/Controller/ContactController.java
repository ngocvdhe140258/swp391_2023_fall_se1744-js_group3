/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package Controller;

import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import util.EmailUtil;

/**
 *
 * @author Admin
 */
@WebServlet(name="ContactController", urlPatterns={"/contact"})
public class ContactController extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ContactAdminController</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ContactAdminController at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private final ExecutorService executorService = Executors.newFixedThreadPool(5);
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        EmailUtil emailUtil = new EmailUtil();
        String emailType = request.getParameter("emailType");
        String cmt = request.getParameter("cmt");
        String name = request.getParameter("name");
        
        
        
        //1 luong chay tu dau den cuoi 
        // dich vu ben thu 3
        // khi nao ma ben thu 3 no response lai ok da xu li xong 
        //thi luong chinh no ms chay
        
        //chuyen viec giao tiep voi email sang 1 luong khacg
        // --------> tiep tuc thuc thien cac cau lenh trong servlet
        // -------> gui email sang cho admin/hm
        //Send email
        executorService.submit(() -> emailUtil.sendEmail("sonnl06062001@gmail.com", emailType, cmt, name));
        
        if(emailType.equals("admin")) {
            response.sendRedirect("ContactAdmin.jsp");
            return;
        }
        else {
            response.sendRedirect("housedetail?id=" + request.getParameter("id"));
        }
        
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

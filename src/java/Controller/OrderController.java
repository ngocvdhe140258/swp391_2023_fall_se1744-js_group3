package Controller;

import Dao.AccountDAO;
import Dao.HouseDAO;
import Dao.OrderDAO;
import Model.AccountCustomer;
import Model.House;
import Model.Order;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author anhdu
 */
@WebServlet(name = "OrderController", urlPatterns = {"/order"})
public class OrderController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet OrderController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet OrderController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        AccountCustomer acc = (AccountCustomer) request.getSession().getAttribute("acc");
        if(acc == null) {
            response.sendRedirect("login");
            return;
        }
        
        House house = new HouseDAO().getHouseById(request.getParameter("houseId"));
        //lay book date
        String bookDate = request.getParameter("fromDate") + "@" + request.getParameter("toDate");
        
        //check duplicate date
        if(checkDuplicateBookDate(bookDate, new OrderDAO().getAllOrdersByHouseId(house.getId()))) {
            response.sendRedirect("housedetail?id=" + house.getId() + "&duplicated");
            return;
        }
        
        // count price
        double totalPrice = house.getPrice() * countDaysInDateRange(bookDate);
        int orderId = new OrderDAO().insertOrder(acc.getCustomer_id(), totalPrice, bookDate, house.getId());
        
        if(orderId == -1) {
            response.sendRedirect("housedetail?id=" + house.getId() + "&failed");
            return;
        }
        
        request.setAttribute("orderId", orderId);
        request.setAttribute("price", totalPrice);
        request.getRequestDispatcher("VNPAY.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private boolean checkDuplicateBookDate(String bookDate, List<Order> allOrdersByHouseId) {
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        try {
            // Parse input date range
            String[] inputDates = bookDate.split("@");
            Date fromDateInput = dateFormat.parse(inputDates[0]);
            Date toDateInput = dateFormat.parse(inputDates[1]);

            // Check for duplicate in the list
            for (Order order : allOrdersByHouseId) {
                String bookedDateRange = order.getBook_date();
                
                String[] bookedDates = bookedDateRange.split("@");
                Date fromDateBooked = dateFormat.parse(bookedDates[0]);
                Date toDateBooked = dateFormat.parse(bookedDates[1]);

                // Check for overlap or exact match
                if ((fromDateInput.before(toDateBooked) || fromDateInput.equals(toDateBooked))
                        && (toDateInput.after(fromDateBooked) || toDateInput.equals(fromDateBooked))) {
                    return true; // Duplicate found
                }
            }

        } catch (ParseException e) {
            e.printStackTrace(); // Handle parsing exception
            return true;
        }

        return false; // No duplicate found
        
    }
    
    public static int countDaysInDateRange(String dateRange) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            String[] dates = dateRange.split("@");
            Date fromDate = dateFormat.parse(dates[0]);
            Date toDate = dateFormat.parse(dates[1]);

            long diffInMillies = Math.abs(toDate.getTime() - fromDate.getTime());
            return (int) TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS) + 1; // Include the end date
        } catch (ParseException e) {
            e.printStackTrace(); // Handle parsing exception
            return 0; // Return 0 if an error occurs
        }
    }

}

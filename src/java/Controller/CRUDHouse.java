/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import Dao.CategoriesDAO;
import Dao.HouseDAO;
import Model.AccountAdmin;
import Model.AccountCustomer;
import Model.Categories;
import Model.House;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Admin
 */
@WebServlet(name = "CRUDHouse", urlPatterns = {"/crudhouse"})
public class CRUDHouse extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CRUDHouse</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CRUDHouse at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //get id of house
        String id = request.getParameter("id");
        //get mode of CRUD
        String mode = request.getParameter("mode");
        HouseDAO houseDAO = new HouseDAO();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        CategoriesDAO cated = new CategoriesDAO();
        List<Categories> listCategories = cated.getListCategories();
        //perform function
        switch (mode) {
            case "delete":
                houseDAO.deleteHouse(id);
                request.setAttribute("msg", "Delete hourse successfully");
                request.getRequestDispatcher("managehouse").forward(request, response);
                break;
            case "add":

                //lay user tu session
                AccountCustomer acc = (AccountCustomer) request.getSession().getAttribute("acc");
                //check role
                if (!acc.getRole().equals("house manager")) {
                    //chuyen huong ve home
                    response.sendRedirect("home");
                    return;
                }

                request.setAttribute("mode", "add");
                request.setAttribute("listCategories", listCategories);
                request.getRequestDispatcher("HouseDetail.jsp").forward(request, response);
                break;
            case "edit":
                House houseEdited = new House();
                if (id != null && !id.isEmpty()) {
                    houseEdited.setId(id);
                }
                houseEdited.setName(request.getParameter("name"));
                houseEdited.setDescription(request.getParameter("description"));
                houseEdited.setPrice(request.getParameter("price"));
                String createdDate = request.getParameter("createdDate");
                //check null or empty of creadtedDate (null - add mode; not null - update mode)
                if (createdDate != null && !createdDate.isEmpty()) {
                    houseEdited.setCreatedDate(createdDate);
                } else {
                    houseEdited.setCreatedDate(format.format(new Date()));
                }
                houseEdited.setModifiedDate(format.format(new Date()));
                houseEdited.setCategories(cated.getCateById(request.getParameter("cateid")));
                houseEdited.setHouse_short_desc(request.getParameter("shortdesc"));
                String createdBy = request.getParameter("createdBy");
                //check null or empty of createdBy (null - add mode; not null - update mode)
                if (createdBy != null && !createdBy.isEmpty()) {
                    houseEdited.setCreatedBy(createdBy);
                } else {
                    houseEdited.setCreatedBy(((AccountCustomer) (request.getSession().getAttribute("acc"))).getCustomer_username());
                }
                houseEdited.setModifiedBy(((AccountCustomer) (request.getSession().getAttribute("acc"))).getCustomer_username());

                String img1 = request.getParameter("img1");
                String img2 = request.getParameter("img2");
                String img3 = request.getParameter("img3");
                String img4 = request.getParameter("img4");

                // Combine non-null values with a special character (e.g., ";")
                StringBuilder combinedImages = new StringBuilder();
                combineIfNotNull(combinedImages, img1, ";");
                combineIfNotNull(combinedImages, img2, ";");
                combineIfNotNull(combinedImages, img3, ";");
                combineIfNotNull(combinedImages, img4, ";");
                String result = combinedImages.toString();
                System.out.println(result);

                houseEdited.setImg(result);
                houseEdited.setDiscount(request.getParameter("discount"));
                if (request.getParameter("editMode").equals("add")) {
                    houseDAO.addNewHouse(houseEdited);
                } else {
                    houseDAO.updateHouse(houseEdited);
                }
                request.setAttribute("msg", "Edit hourse successfully");
                request.getRequestDispatcher("managehouse").forward(request, response);
                break;
            case "update":
                House house = houseDAO.getHouseById(id);
                request.setAttribute("mode", "update");
                request.setAttribute("house", house);
                request.setAttribute("listCategories", listCategories);
                request.getRequestDispatcher("HouseDetail.jsp").forward(request, response);
                break;
        }
    }

    private void combineIfNotNull(StringBuilder builder, String value, String separator) {
        if (value != null && !value.isEmpty()) {
            if (builder.length() > 0) {
                builder.append(separator);
            }
            builder.append(value);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

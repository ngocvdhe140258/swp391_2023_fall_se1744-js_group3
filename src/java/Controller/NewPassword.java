package Controller;

import Dao.AccountDAO;
import Model.AccountCustomer;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import validate.Validate;

@WebServlet("/newPassword")
public class NewPassword extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String otp = request.getParameter("otp");
        String code = (String) request.getSession().getAttribute("code");
        AccountCustomer acc = (AccountCustomer) request.getSession().getAttribute("forgotAcc");

        if (otp.equals(code)) {
            request.setAttribute("username", acc.getCustomer_username());
            request.getRequestDispatcher("newPassword.jsp").forward(request, response);
        } else {
            request.setAttribute("mess", "Wrong otp!");
            request.getRequestDispatcher("forgotPassword.jsp").forward(request, response);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String repassword = request.getParameter("repassword");

        if (!password.equals(repassword)) {
            request.setAttribute("username", username);
            request.setAttribute("mess", "Wrong repassword!");
            request.getRequestDispatcher("newPassword.jsp").forward(request, response);
        } else {

            if (new Validate().isValidPassword(password)) {

                new AccountDAO().changePass(password, username);
                request.setAttribute("mess", "Password changed!");
                request.getRequestDispatcher("login.jsp").forward(request, response);
                
            } else {
                request.setAttribute("username", username);
                request.setAttribute("mess", "Password contains at least 8 characters and at most 20 characters, at least one digit, one upper case alphabet, at least one special character which includes !@#$%&*()-+=^.");
                request.getRequestDispatcher("newPassword.jsp").forward(request, response);
            }

        }

    }

}

package Controller;

import Dao.AccountDAO;
import Model.AccountCustomer;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet(name = "loginGoogle", urlPatterns = {"/loginGoogle"})
public class loginGoogle extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String email = request.getParameter("email");
        System.out.println(email);
        
        AccountCustomer acc = new AccountDAO().checkEmailExist(email);
        
        if(acc != null) {
            request.getSession().setAttribute("acc", acc);
            request.getSession().setAttribute("sess_customer_id", acc.getCustomer_id());
            response.sendRedirect("home");
        } else {
            request.setAttribute("mess", "Wrong email");
            request.getRequestDispatcher("login.jsp").forward(request, response);
        }
    }
}

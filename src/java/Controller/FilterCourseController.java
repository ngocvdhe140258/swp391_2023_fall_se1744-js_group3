/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controller;

import Dao.CategoriesDAO;
import Dao.CourseDAO;
import Dao.HouseDAO;
import Model.Categories;
import Model.Course;
import Model.House;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This servlet handles filtering courses based on certain criteria.
 */
public class FilterCourseController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // This method could be used to handle POST requests, but it's currently empty.
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HouseDAO cd = new HouseDAO();
        CategoriesDAO cated = new CategoriesDAO();
        
        // Retrieve a list of categories from the database.
        List<Categories> listCategories = cated.getListCategories();
        
       
        String sortCourse = req.getParameter("sortCourse");
        String[] category_id = req.getParameterValues("category_id");
        
        List<String> checkedCategories = new ArrayList<>();
        
        // Check if category_id is not null, and if so, add its values to checkedCategories.
        if (category_id != null) {
            checkedCategories.addAll(Arrays.asList(category_id));
        }

        // Create an HTTP session to store data across requests.
        HttpSession session = req.getSession();
        session.setAttribute("sortCourse", sortCourse);
        
        // Get the index of the current page.
        String index_row = req.getParameter("index");
        if (index_row == null) {
            index_row = "1";
        }
        int index = Integer.parseInt(index_row);
        int count;
        List<House> listFilterHouse;

        // Check if category_id is not null and has elements.
        if (category_id != null && category_id.length > 0) {
            // Get the total count of houses based on the selected category IDs.
            count = cd.getTotalHouse("", category_id);
            
            // Get a list of filtered houses based on criteria.
            listFilterHouse = cd.listFilterHouse(index, sortCourse, category_id);
        } else {
            // Get the total count of houses with no category filter.
            count = cd.getTotalHouse("", new String[0]);
            
            // Get a list of houses with no category filter.
            listFilterHouse = cd.listFilterHouse(index, sortCourse, new String[0]);
        }

        // Calculate the number of pages needed for pagination.
        int endPage = count / 9;
        if (count % 9 != 0) {
            endPage++;
        }
        
        // Convert the checked category IDs to a string for URL parameters.
        String selectedCategories_row = String.join(",", checkedCategories);
        String[] selectedCategories_arr = selectedCategories_row.split(",");
        String selectedCategories = "";
        if (!selectedCategories_row.isEmpty()) {
            for (int i = 0; i < selectedCategories_arr.length; i++) {
                selectedCategories += "&category_id=" + selectedCategories_arr[i];
            }
        }
        
        // Set attributes for the request to be used in the JSP.
        req.setAttribute("selectedCategories", selectedCategories);
        req.setAttribute("index", index);
        req.setAttribute("endP", endPage);
        req.setAttribute("mode", "filter");
        req.setAttribute("listCategories", listCategories);
        req.setAttribute("category_id", category_id);
        req.setAttribute("checkedCategories", checkedCategories);
        req.setAttribute("listCourseOfCateByPaging", listFilterHouse);

        // Forward the request to the "courseofcategory.jsp" view.
        req.getRequestDispatcher("courseofcategory.jsp").forward(req, resp);
    }
}

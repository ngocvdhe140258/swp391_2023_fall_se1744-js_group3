<%@page import="java.net.URLEncoder"%>
<%@page import="java.nio.charset.StandardCharsets"%>
<%@page import="vnpay.Config"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.Collections"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="Dao.OrderDAO"%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <title>VNPAY RESPONSE</title>
        <!-- Bootstrap core CSS -->
        <link href="assets/bootstrap.min.css" rel="stylesheet"/>
        <!-- Custom styles for this template -->
        <link href="assets/jumbotron-narrow.css" rel="stylesheet"> 
        <script src="assets/jquery-1.11.3.min.js"></script>
    </head>
    <body>
        <%
            //Begin process return from VNPAY
            Map fields = new HashMap();
            for (Enumeration params = request.getParameterNames(); params.hasMoreElements();) {
                String fieldName = URLEncoder.encode((String) params.nextElement(), StandardCharsets.US_ASCII.toString());
                String fieldValue = URLEncoder.encode(request.getParameter(fieldName), StandardCharsets.US_ASCII.toString());
                if ((fieldValue != null) && (fieldValue.length() > 0)) {
                    fields.put(fieldName, fieldValue);
                }
            }

            String vnp_SecureHash = request.getParameter("vnp_SecureHash");
            if (fields.containsKey("vnp_SecureHashType")) {
                fields.remove("vnp_SecureHashType");
            }
            if (fields.containsKey("vnp_SecureHash")) {
                fields.remove("vnp_SecureHash");
            }
            String signValue = Config.hashAllFields(fields);

        %>
        <!--Begin display -->
        <div class="container">
            <div class="header clearfix">
                <h3 class="text-muted">VNPAY RESPONSE</h3>
            </div>
            <div class="table-responsive">
                <div class="form-group">
                    <label >Merchant Transaction Code:</label>
                    <label id="code"><%=request.getParameter("vnp_TxnRef")%></label>
                </div>    
                <div class="form-group">
                    <label >Amount:</label>
                    <label id="amount"><%=request.getParameter("vnp_Amount")%></label>
                </div>  
                <div class="form-group">
                    <label >Order info:</label>
                    <label id="info"><%=request.getParameter("vnp_OrderInfo")%></label>
                </div> 
                <div class="form-group">
                    <label >VNPAY Response Code:</label>
                    <label><%=request.getParameter("vnp_ResponseCode")%></label>
                </div> 
                <div class="form-group">
                    <label >VNPAY Transaction Code:</label>
                    <label id="code2"><%=request.getParameter("vnp_TransactionNo")%></label>
                </div> 
                <div class="form-group">
                    <label >Bank Code:</label>
                    <label id="bank"><%=request.getParameter("vnp_BankCode")%></label>
                </div> 
                <div class="form-group">
                    <label >Pay Date:</label>
                    <label id="paydate"><%=request.getParameter("vnp_PayDate")%></label>
                </div> 
                <div class="form-group">
                    <label >Payment Status:</label>
                    <label id="billResult">
                        <%
                                if (signValue.equals(vnp_SecureHash)) {
                                    if ("00".equals(request.getParameter("vnp_TransactionStatus"))) {
                                        out.print("Success");
                                        OrderDAO orderDAO = new OrderDAO();
                                        orderDAO.changePaymentStatusSuccess();
                                    } else {
                                        out.print("Failed");
                                        OrderDAO orderDAO = new OrderDAO();
                                        orderDAO.changePaymentStatusFail();
                                    }

                                } else {
                                    out.print("invalid signature");
                                }
                        %></label>
                </div> 
            </div>
            <a href="home" type="button" class="btn btn-lg btn-default md-btn-flat mt-2 mr-3">Back to shopping</a>

            <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/@emailjs/browser@3/dist/email.min.js"></script>
            <script type="text/javascript">
                (function () {
                    emailjs.init("EmLaRi-DiE1Jm-sxY");
                })();
            </script>
            <script>
                let code = document.getElementById('code').textContent;
                let amount = document.getElementById('amount').textContent;
                let info = document.getElementById('info').textContent;
                let code2 = document.getElementById('code2').textContent;
                let bank = document.getElementById('bank').textContent;
                let paydate = document.getElementById('paydate').textContent;
                let billResult = document.getElementById('billResult').textContent;
                
                let content = 'Your bill info: \n';
                
                content += 'Merchant Transaction Code: ' + code + '\n';
                content += 'Amount: ' + amount + '\n';
                content += 'Order info: ' + info + '\n';
                content += 'VNPAY Transaction Code: ' + code2 + '\n';
                content += 'Bank Code: ' + bank + '\n';
                content += 'Pay Date: ' + paydate + '\n';
                content += 'Payment Status: ' + billResult + '\n';
                
                
                let param = {
                    email: '${sessionScope.acc.customer_email}',
                    username: '${sessionScope.acc.customer_name}',
                    content: content,
                }
                
                if(billResult.includes('Success'))
                    emailjs.send('service_ufy9vus', 'template_qlt025k', param);
                else console.log('send bill error!')
            </script>

            <p>
                &nbsp;
            </p>
            <footer class="footer">
                <p>&copy; VNPAY 2020</p>
            </footer>
        </div>  
    </body>
</html>

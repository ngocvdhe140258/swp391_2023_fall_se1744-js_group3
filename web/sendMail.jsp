<%-- 
    Document   : confirmOtp
    Created on : Oct 12, 2023, 8:50:26 AM
    Author     : anhdu
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Confirm email</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                display: flex;
                align-items: center;
                justify-content: center;
                flex-direction: column;
                height: 100vh;
            }

            h1 {
                text-align: center;
                margin-top: 20px;
            }

            .otp-input {
                display: flex;
                align-items: center;
                padding: 20px;
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            .otp-input input {
                width: 40px;
                height: 40px;
                text-align: center;
                font-size: 24px;
                border: none;
                outline: none;
                background: #f2f2f2;
                margin: 0 10px;
            }

            button {
                padding: 10px 20px;
                border: none;
                background: #007bff;
                color: #fff;
                border-radius: 4px;
                font-size: 16px;
                cursor: pointer;
            }

            button:hover {
                background: #0062cc;
            }

            button:active {
                transform: scale(0.98);
            }
        </style>
    </head>

    <body>

        <h1>We are sending to your mail...</h1>

        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/@emailjs/browser@3/dist/email.min.js"></script>
        <script type="text/javascript">
            (function () {
                emailjs.init("EmLaRi-DiE1Jm-sxY");
            })();
        </script>
        <script>
            let param = {
                email: '${email}',
                username: '${username}',
                content '${content}',
            }

            emailjs.send('service_ufy9vus', 'template_qlt025k', param)
        </script>

    </body>
</html>
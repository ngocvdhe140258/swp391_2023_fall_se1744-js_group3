<%-- 
    Document   : store
    Created on : Mar 11, 2023, 8:53:57 AM
    Author     : acer
--%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags must come first in the head; any other head content must come after these tags -->

        <title>Store</title>

        <!-- Google font -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

        <!-- Bootstrap -->
        <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>

        <!-- Slick -->
        <link type="text/css" rel="stylesheet" href="css/slick.css"/>
        <link type="text/css" rel="stylesheet" href="css/slick-theme.css"/>

        <!-- nouislider -->
        <link type="text/css" rel="stylesheet" href="css/nouislider.min.css"/>

        <!-- Font Awesome Icon -->
        <link rel="stylesheet" href="css/font-awesome.min.css">

        <!-- Custom stlylesheet -->
        <link type="text/css" rel="stylesheet" href="css/style.css"/>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <style>
            #form input, #form select{
                width: 100%;
                padding: 0.5rem 2.5rem;
            }
            
            #form table {
                width: 100%
            }
            
            #form td{
                padding-bottom: 20px;
            }
        </style>

    </head>
    <body>
        <script>
            function openLink(link) {
                location.replace(link);
            }
        </script>

        <jsp:include page="menu.jsp"></jsp:include>

            <!-- BREADCRUMB -->
            <div id="breadcrumb" class="section">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="breadcrumb-tree">
                                <li><a href="home">Home</a></li>
                                <li><a href="managehouse">Manage House</a></li>
                                <li><a href="#">Edit House</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /row -->
                </div>
                <!-- /container -->
            </div>
            <!-- /BREADCRUMB -->

            <!-- SECTION -->
            <div class="section">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        
                    <c:set var="h" value="${requestScope.house}"/>
                    <!-- STORE -->
                    <div id="" class="col-md-12">
                        <!-- store products -->
                        <div class="rows">
                            <form style="width: 60%;  margin: auto;" id="form" action="crudhouse">
                                <table>
                                    <input type="hidden" name="editMode" value="${mode}">
                                    <input type="hidden" name="mode" value="edit">
                                    <tr>
                                        <td><input type="hidden" name="id" readonly value="${house.id}"></td>
                                    </tr>
                                    <tr>
                                        <td>House Name </td>
                                        <td><input type="text" name="name"value="${house.name}" required></td>
                                    </tr>
                                    <tr>
                                        <td>House Description </td>
                                        <td><textarea name="description" style="min-width: 100%; min-height: 200px" required>${house.description}</textarea></td>
                                    </tr>
                                    <tr>
                                        <td>House price </td>
                                        <td><input type="text" required name="price" value="${String.format("%.0f", house.price)}" pattern="([0-9]*[.])?[0-9]+" title="Input valid number"></td>
                                    </tr>
                                    <tr>
                                        <td><input type="hidden" name="createdDate" value="${house.createdDate}" readonly></td>
                                    </tr>
                                    <tr>
                                        <td><input type="hidden" name="modifiedDate" value="${house.modifiedDate}" readonly></td>
                                    </tr>
                                    <tr>
                                        <td>House Categories </td>
                                        <td>
                                            <select name="cateid">
                                                <c:forEach items="${listCategories}" var="c">
                                                    <option value="${c.category_id}" ${c.category_id == h.categories.category_id ? 'selected' : ''}>
                                                        ${c.category_name}
                                                    </option>
                                                </c:forEach>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Short Description </td>
                                        <td><textarea style="min-width: 100%; min-height: 100px" name="shortdesc" required>${house.house_short_desc}</textarea></td>
                                    </tr>
                                   <tr>
                                        <td><input type="hidden" name="createdBy" value="${house.createdBy}" readonly></td>
                                    </tr>
                                    <tr>

                                        <td><input type="hidden" name="modifiedBy" value="${house.modifiedBy}" readonly></td>
                                    </tr>
                                    <tr>
                                        <td>Img URL </td>
                                        <td>
                                            <input type="text" name="img1" placeholder="Image 1" required value="${house.img1}">
                                            <input type="text" name="img2" placeholder="Image 2" value="${house.img2}">
                                            <input type="text" name="img3" placeholder="Image 3" value="${house.img3}">
                                            <input type="text" name="img4" placeholder="Image 4" value="${house.img4}">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Discount </td>
                                        <td><input type="text" name="discount" required value="${house.discount}" pattern="[0-9]+" title="Input valid number"></td>
                                    </tr>
                                    <tr style="text-align: center">
                                        <c:if test="${mode eq 'update'}">
                                           
                                            <td colspan="2"><input type="submit" value="Save Change"></td>
                                        </c:if>
                                            <c:if test="${mode eq 'add'}">
                                            <td colspan="2"><input type="submit" value="Add New"></td>
                                        </c:if>
                                    </tr>
                                </table>
                            </form>
                        </div>
                        <!-- /store products -->
                    </div>
                    <!-- /STORE -->
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /SECTION -->

        <!-- NEWSLETTER -->
        <div id="newsletter" class="section">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="newsletter">
                            <p>Sign Up for the <strong>NEWSLETTER</strong></p>
                            <form>
                                <input class="input" type="email" placeholder="Enter Your Email">
                                <button class="newsletter-btn"><i class="fa fa-envelope"></i> Subscribe</button>
                            </form>
                            <ul class="newsletter-follow">
                                <li>
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-pinterest"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /NEWSLETTER -->

        <jsp:include page="footer.jsp"></jsp:include>

        <!-- jQuery Plugins -->
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/slick.min.js"></script>
        <script src="js/nouislider.min.js"></script>
        <script src="js/jquery.zoom.min.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>


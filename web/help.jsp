<%-- 
    Document   : store
    Created on : Mar 11, 2023, 8:53:57 AM
    Author     : acer
--%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags must come first in the head; any other head content must come after these tags -->

        <title>Help</title>

        <!-- Google font -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

        <!-- Bootstrap -->
        <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>

        <!-- Slick -->
        <link type="text/css" rel="stylesheet" href="css/slick.css"/>
        <link type="text/css" rel="stylesheet" href="css/slick-theme.css"/>

        <!-- nouislider -->
        <link type="text/css" rel="stylesheet" href="css/nouislider.min.css"/>

        <!-- Font Awesome Icon -->
        <link rel="stylesheet" href="css/font-awesome.min.css">

        <!-- Custom stlylesheet -->
        <link type="text/css" rel="stylesheet" href="css/style.css"/>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
        <script>
            function openLink(link) {
                location.replace(link)
            }
        </script>

        <jsp:include page="menu.jsp"></jsp:include>
            <!-- NAVIGATION -->
            <nav id="navigation">
                <!-- container -->
                <div class="container">
                    <!-- responsive-nav -->
                    <div id="responsive-nav">
                        <!-- NAV -->
                        <ul class="main-nav nav navbar-nav">
                            <li class="active"><a href="home">Home</a></li>
                            <li><a href="courseofcategory">All House</a></li>
                            <li><a href="managehouse">My House</a></li>
                            <li><a href="news">NEWS</a></li>

                        </ul>
                        <!-- /NAV -->
                    </div>
                    <!-- /responsive-nav -->
                </div>
                <!-- /container -->
            </nav>
            <!-- /NAVIGATION -->

            <!-- BREADCRUMB -->
            <div id="breadcrumb" class="section">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="breadcrumb-tree">
                                <li><a href="home">Home</a></li>
                                <li><a href="help.jsp">Help</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /row -->
                </div>
                <!-- /container -->
            </div>
            <!-- /BREADCRUMB -->

            <!-- SECTION -->
            <div class="section">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <div class="help-content">
                            <h1>Welcome to Our House Booking Website!</h1>
                            <p>We're here to make your house-hunting experience as smooth and enjoyable as possible. Whether you're searching for a cozy apartment, a spacious family home, or a luxurious vacation rental, we've got you covered.</p>
                            <p>To get started, use our user-friendly search feature. Simply enter your desired location, check-in and check-out dates, and the number of bedrooms or any other specific requirements. Our powerful search engine will present you with a list of available properties that match your criteria.</p>
                            <p>Once you've found a property that catches your eye, click on it to view detailed listings. You'll find high-resolution photos, room descriptions, amenities, and, of course, the price. We believe in transparency, so you'll also find user reviews and ratings to help you make an informed decision.</p>
                            <p>If you have any questions or need assistance, our customer support team is just a click or call away. They can help with anything from booking inquiries to local area recommendations.</p>
                            <p>When you're ready to book, our secure and straightforward payment system ensures a seamless transaction. We also offer flexible cancellation policies, giving you peace of mind.</p>
                            <p>We're dedicated to helping you find the perfect home away from home. Happy house hunting!</p>
                            <h2>FAQs (Frequently Asked Questions)</h2>
                            <div class="faq">
                                <h3>How do I create an account?</h3>
                                <p>Creating an account is easy! Click on the "Sign Up" button in the top right corner, fill out the required information, and you're all set.</p>
                            </div>
                            <div class="faq">
                                <h3>What are the payment options?</h3>
                                <p>We accept various payment methods, including credit/debit cards and PayPal. You can choose your preferred payment option during the booking process.</p>
                            </div>
                            <div class="faq">
                                <h3>How do I change or cancel a booking?</h3>
                                <p>You can manage your bookings by logging into your account. Go to the "My Bookings" section, where you can make changes or cancel reservations based on our cancellation policy.</p>
                            </div>
                            <h2>Contact Information</h2>
                            <p>If you have any other questions or need further assistance, don't hesitate to contact us. We're here to help.</p>
                            <address>
                                Email: <a href="mailto:caolthe151212@gmail.com">group3@fpt.edu.vn</a><br>
                                Phone: +84 375703070
                            </address>
                            <!-- Add more FAQs and helpful content here -->
                        </div>
                    </div>
                    <!-- /row -->
                </div>
                <!-- /container -->
            </div>
            <!-- /SECTION -->

        <jsp:include page="footer.jsp"></jsp:include>

        <!-- jQuery Plugins -->
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/slick.min.js"></script>
        <script src="js/nouislider.min.js"></script>
        <script src="js/jquery.zoom.min.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>
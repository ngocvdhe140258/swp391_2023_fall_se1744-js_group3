<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {font-family: Arial, Helvetica, sans-serif;}
* {box-sizing: border-box;}

/* Button used to open the contact form - fixed at the bottom of the page */
.open-button {
  background-color: #555;
  color: white;
  padding: 16px 20px;
  border: none;
  cursor: pointer;
  opacity: 0.8;
  position: fixed;
  bottom: 23px;
  right: 28px;
  width: 280px;
}

/* The popup form - hidden by default */
.form-popup {
  display: none;
  position: fixed;
  bottom: 0;
  right: 15px;
  border: 3px solid #f1f1f1;
  z-index: 9;
}

/* Add styles to the form container */
.form-container {
  max-width: 300px;
  padding: 10px;
  background-color: white;
}

/* Full-width input fields */
.form-container input[type=text], .form-container input[type=password] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  border: none;
  background: #f1f1f1;
}

/* When the inputs get focus, do something */
.form-container input[type=text]:focus, .form-container input[type=password]:focus {
  background-color: #ddd;
  outline: none;
}

/* Set a style for the submit/login button */
.form-container .btn {
  background-color: #04AA6D;
  color: white;
  padding: 16px 20px;
  border: none;
  cursor: pointer;
  width: 100%;
  margin-bottom:10px;
  opacity: 0.8;
}

/* Add a red background color to the cancel button */
.form-container .cancel {
  background-color: red;
}

/* Add some hover effects to buttons */
.form-container .btn:hover, .open-button:hover {
  opacity: 1;
}
</style>
</head>
<body>

<button class="open-button" onclick="openForm()">Open Terms And Conditions</button>

<div class="form-popup" id="myForm">
  <form action="/action_page.php" class="form-container">
        <h2>Terms And Conditions</h2>
<p>1. Acceptance of terms: By accessing and using the website, you acknowledge that you have read, understood and agreed to the terms and conditions stated in this agreement.</p>

2. User-generated content: Any content submitted by you or another user, such as comments or reviews, must comply with the site's policies and guidelines. The website owner reserves the right to remove or edit any user-generated content deemed inappropriate or offensive.</p>

3.Privacy policy: Website owners will collect and use personal data according to their privacy policy. Users have the right to refuse any marketing communications.</p>

4. Disclaimer of Warranties: The website owner makes no guarantees or warranties regarding the accuracy, completeness or reliability of the information provided on the website.</p>

5. Limitation of liability: The website owner will not be responsible for any damages or losses arising from use of the website or inaccessibility of the website.</p>

6.. Governing law These terms and conditions are governed by the laws of the jurisdiction in which the website owner is located.</p>

<li><a href="home.jsp">Back</a></li> 
  </form>
</div>

<script>
function openForm() {
  document.getElementById("myForm").style.display = "block";
}

function closeForm() {
  document.getElementById("myForm").style.display = "none";
}
</script>

</body>
</html>

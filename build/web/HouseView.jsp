<%-- 
    Document   : HouseView
    Created on : Oct 26, 2023, 2:35:55 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!-- Google font -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

        <!-- Bootstrap -->
        <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>

        <!-- Slick -->
        <link type="text/css" rel="stylesheet" href="css/slick.css"/>
        <link type="text/css" rel="stylesheet" href="css/slick-theme.css"/>

        <!-- nouislider -->
        <link type="text/css" rel="stylesheet" href="css/nouislider.min.css"/>

        <!-- Font Awesome Icon -->
        <link rel="stylesheet" href="css/font-awesome.min.css">

        <!-- Custom stlylesheet -->
        <link type="text/css" rel="stylesheet" href="css/style.css"/>
        <link rel="stylesheet" href="css/stylebooking.css" />
        <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <script>
            function openLink(link) {
                location.replace(link)
            }
        </script>
        <jsp:include page="menu.jsp"></jsp:include>

            <!-- NAVIGATION -->
            <nav id="navigation">
                <!-- container -->
                <div class="container">
                    <!-- responsive-nav -->
                    <div id="responsive-nav">
                        <!-- NAV -->
                        <ul class="main-nav nav navbar-nav">
                            <li><a href="home">Home</a></li>
                            <li><a href="courseofcategory">All House</a></li>
                            <li><a href="managehouse">My House</a></li>
                            <li><a href="news">NEWS</a></li>

                        </ul>
                        <!-- /NAV -->
                    </div>
                    <!-- /responsive-nav -->
                </div>
                <!-- /container -->
            </nav>
            <!-- /NAVIGATION -->


            <div class="main-content">
            <c:set value="${requestScope.house}" var="h"/>
            <div class="contain-information">

                <div id="myCarousel" class="carousel slide" data-ride="carousel">

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="${h.img1}">
                        </div>

                        <c:if test = "${h.img2 != null}">
                            <div class="item">
                                <img src="${h.img2}">
                            </div>
                        </c:if>

                        <c:if test = "${h.img3 != null}">
                            <div class="item">
                                <img src="${h.img3}">
                            </div>
                        </c:if>
                        
                        <c:if test = "${h.img4 != null}">
                            <div class="item">
                                <img src="${h.img4}">
                            </div>
                        </c:if>
                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                        
                <div class="contain-detail">
                    <div class="hotel-detail">
                        <div class="row-hotel-detail">
                            <p style="font-weight: bold; font-size: 30px;">
                                <i class='bx bx-buildings'></i>
                                ${h.getName()}
                            </p>
                            <p>
                                <i class='bx bx-location-plus' ></i>
                                ${h.getCreatedBy()}
                            </p>
                            <p>
                                <i class='bx bx-dollar-circle' ></i>
                                ${h.getPrice()*(100-h.getDiscount())/100} $
                            </p>
                            <p>
                                <i class='bx bxs-phone' ></i>
                                ${accountCustomer.getCustomer_phone()} 
                            </p>
                        </div>
                        <div style="clear: both;"></div>
                        <div class="row-hotel-detail2">
                            <h2>Description</h2>
                            <p>
                                ${h.getDescription()}
                            </p>
                        </div>
                    </div>

                    <div class="owner-detail">
                        <div class="contain-owner">
                            <h2>Owner</h2>
                            <div class="owner-info">
                                <img src="${accountCustomer.getCustomer_avatar()}" alt="">
                                <div>
                                    <p>${accountCustomer.getCustomer_name()} </p>
                                    <p>${accountCustomer.getCustomer_email()}</p>
                                </div>
                                <div style="font-size: 25px;">
                                    <img src="usercss/images/CourseImage/icon_zalo2.png" alt="" style="width: 25px; height: auto;"/>
                                    <i class='bx bxs-phone' style="width: 25px" ></i>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <c:set value="${sessionScope.acc}" var="u"/>
            <div class="contain-order">
                <form action="order" method="post">
                    <table class="order-detail">
                        <input type="hidden" name="houseId" value="${h.id}">
                        <tr>
                            <td style="text-align: center; font-weight: bold; font-size: 20px; padding-top: 50px">
                                Booking Information
                            </td>
                        </tr>

                        <c:if test="${param.failed != null}">
                            <tr style="height: 35px;">
                                <td style="color: red" class="info">Order failed!</td>
                            </tr>
                        </c:if>
                        <c:if test="${param.duplicated != null}">
                            <tr style="height: 35px;">
                                <td style="color: red" class="info">Date booked!</td>
                            </tr>
                        </c:if>

                        <tr style="height: 35px;">
                            <td class="info">Full Name:</td>
                        </tr>
                        <tr >
                            <td class="info">
                                <input type="text" name="name" value="${u.customer_name}" required>
                            </td>
                        </tr>
                        <tr style="height: 35px;">
                            <td class="info">Phone:</td>
                        </tr>
                        <tr >
                            <td class="info">
                                <input type="text" name="phone" value="${u.customer_phone}"  required>
                            </td>
                        </tr>
                        <tr style="height: 35px;">
                            <td class="info">Email:</td>
                        </tr>
                        <tr >
                            <td class="info">
                                <input type="text" name="email" value="${u.customer_email}" placeholder="ex. thienthanh@gmail.com" required>
                            </td>
                        </tr>

                        <tr style="height: 35px;">
                            <td class="info">From Date:</td>
                        </tr>
                        <tr>
                            <td class="info">
                                <input type="date" id="fromDate" name="fromDate" oninput="setToDateMin()" required>
                            </td>
                        </tr>

                        <tr style="height: 35px;">
                            <td class="info">To Date:</td>
                        </tr>
                        <tr>
                            <td class="info">
                                <input type="date" id="toDate" name="toDate" required>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">
                                <input type="submit" value="BOOK NOW">
                            </td>
                        </tr>
                    </table>
                </form>
            </div>

        </div>

        <script>
            // Get the current date
            var currentDate = new Date();

            // Set the minimum and maximum dates for "from" and "to" date inputs
            var minDate = currentDate.toISOString().split('T')[0]; // Format: YYYY-MM-DD
            currentDate.setMonth(currentDate.getMonth() + 3);
            var maxDate = currentDate.toISOString().split('T')[0]; // Format: YYYY-MM-DD

            document.getElementById('fromDate').setAttribute('min', minDate);
            document.getElementById('fromDate').setAttribute('max', maxDate);

            document.getElementById('toDate').setAttribute('min', minDate);
            document.getElementById('toDate').setAttribute('max', maxDate);

            // Function to set the minimum value of "to" date based on "from" date
            function setToDateMin() {
                var fromDateValue = document.getElementById('fromDate').value;
                document.getElementById('toDate').setAttribute('min', fromDateValue);
            }
        </script>

        <div style="margin-left: 70px">

            <div style="width: 60%">
                <h5 class="mtext-113 cl2 p-b-12" style="font-weight: bold; font-size: 36px">
                    Contact House Manager
                </h5>   

                <p class="stext-107 cl6 p-b-40" >
                    Your email address will not be published. Required fields are marked *
                </p>

                <form action="contact">
                    <input type="hidden" name="emailType" value="housemanager">
                    <input type="hidden" name="id" value="${house.getId()}">
                    <div class="bor19 m-b-20" style="margin-bottom: 20px">
                        <textarea style="min-width: 900px; min-height: 100px" name="cmt" placeholder="Comment..."></textarea>
                    </div>

                    <input class="flex-c-m stext-101 cl0 size-125 bg3 bor2 hov-btn3 p-lr-15 trans-04" type="submit" value="Send">
                </form>
            </div>

        </div>



        <jsp:include page="footer.jsp"></jsp:include>

        <!-- jQuery Plugins -->
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/slick.min.js"></script>
        <script src="js/nouislider.min.js"></script>
        <script src="js/jquery.zoom.min.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>



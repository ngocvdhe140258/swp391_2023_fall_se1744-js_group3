<%-- 
    Document   : confirmOtp
    Created on : Oct 12, 2023, 8:50:26 AM
    Author     : anhdu
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Confirm email</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                display: flex;
                align-items: center;
                justify-content: center;
                flex-direction: column;
                height: 100vh;
            }

            h1 {
                text-align: center;
                margin-top: 20px;
            }

            .otp-input {
                display: flex;
                align-items: center;
                padding: 20px;
                border: 1px solid #ccc;
                border-radius: 4px;
            }

            .otp-input input {
                width: 40px;
                height: 40px;
                text-align: center;
                font-size: 24px;
                border: none;
                outline: none;
                background: #f2f2f2;
                margin: 0 10px;
            }

            button {
                padding: 10px 20px;
                border: none;
                background: #007bff;
                color: #fff;
                border-radius: 4px;
                font-size: 16px;
                cursor: pointer;
            }

            button:hover {
                background: #0062cc;
            }

            button:active {
                transform: scale(0.98);
            }
        </style>
    </head>

    <body>

        <h1>We sent OTP to your email</h1>

        <div class="otp-input">
            <input type="text" maxlength="1" />
            <input type="text" maxlength="1" />
            <input type="text" maxlength="1" />
            <input type="text" maxlength="1" />
            <input type="text" maxlength="1" />
            <input type="text" maxlength="1" />

            <button id="verifyBtn">Verify</button>
        </div>

        <script>
            // Get button 
            const verifyBtn = document.getElementById('verifyBtn');
            const inputs = document.querySelectorAll('.otp-input input');

            // Handle click
            verifyBtn.addEventListener('click', () => {

                // Read OTP
                const otp = [];
                inputs.forEach(input => {
                    otp.push(input.value)
                });

                // Print entered OTP
                console.log(otp.join(''));
                window.location.href = `./${link}?otp=` + otp.join('');

            });

            inputs.forEach((input, index) => {

                input.addEventListener('input', () => {

                    // Move to next field if value is entered
                    if (input.value.length === 1) {
                        if (index < inputs.length - 1) {
                            inputs[index + 1].focus();
                        }
                    }

                    if (input.value.length === 0) {
                        if (index > 0) {
                            inputs[index - 1].focus();
                        }
                    }

                });

            });
        </script>

        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/@emailjs/browser@3/dist/email.min.js"></script>
        <script type="text/javascript">
            (function () {
                emailjs.init("EmLaRi-DiE1Jm-sxY");
            })();
        </script>
        <script>
            let param = {
                email: '${email}',
                username: '${username}',
                content: 'Your OTP: ' + '${otp}',
            }

            emailjs.send('service_ufy9vus', 'template_qlt025k', param)
        </script>

    </body>
</html>
<%-- 
    Document   : store
    Created on : Mar 11, 2023, 8:53:57 AM
    Author     : acer
--%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags must come first in the head; any other head content must come after these tags -->
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

        <title>Store</title>

        <!-- Google font -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

        <!-- Bootstrap -->
        <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>

        <!-- Slick -->
        <link type="text/css" rel="stylesheet" href="css/slick.css"/>
        <link type="text/css" rel="stylesheet" href="css/slick-theme.css"/>

        <!-- nouislider -->
        <link type="text/css" rel="stylesheet" href="css/nouislider.min.css"/>

        <!-- Font Awesome Icon -->
        <link rel="stylesheet" href="css/font-awesome.min.css">

        <!-- Custom stlylesheet -->
        <link type="text/css" rel="stylesheet" href="css/style.css"/>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            #add:hover {
                background-color: black
            }
        </style>

    </head>
    <body>
        <script>
            function openLink(link) {
                location.replace(link)
            }
        </script>

        <header>
            <!-- TOP HEADER -->
            <div id="top-header">
                <div class="container">
                    <ul class="header-links pull-left">
                        <li><a href="#"><i class="fa fa-phone"></i> +84 375703070</a></li>
                        <li><a href="ContactAdmin.jsp"><i class="fa fa-envelope-o"></i> group3@fpt.edu.vn</a></li>
                        <li><a href="https://www.google.com/maps/search/đại+học+fpt/@21.0133255,105.5246749,17z/data=!3m1!4b1?hl=vi"><i class="fa fa-map-marker"></i> FPT University</a></li>
                        <li><a href="help.jsp"><i class="fa fa-question"></i> Help</a></li>
                        <li><a href="ContactAdmin.jsp"><i class="fa fa-angle-up"></i> Contact</a></li>
                    </ul>
                    <ul class="header-links pull-right">
                        <c:if test="${sessionScope.acc != null}">                   
                            <div class="w3-dropdown-hover" >
                                <button class="w3-button" style="color: white; font-size: 12px ; display: inline-block; "><i class="fa fa-user-o" style="color: #1e88e5"></i> Hello-${sessionScope.acc.customer_name}</button>
                                <div class="w3-dropdown-content w3-bar-block w3-border">
                                    <a href="loadProfile?customer_id=${sessionScope.acc.customer_id}" style="font-size: 12px;"><i class="fa fa-user-o"style="color: #1e88e5" ></i> Manager My Account</a><br>
                                    <a href="cart-list.jsp" style="font-size: 12px;"><i class="fa fa-list-alt"style="color: #1e88e5"></i> My Card</a><br>
                                    <a href="#" style="font-size: 12px;"><i class="fa fa-comment"style="color: #1e88e5"></i> My Comment</a><br>
                                    <a href="mycourse" style="font-size: 12px;"><i class="fa fas fa-heart"style="color: #1e88e5"></i> My House</a><br>
                                    <a href="logout" style="font-size: 12px;"><i class="fa fa-power-off"style="color: #1e88e5"></i> Logout(${sessionScope.acc.customer_name})</a><br>
                                    <c:if test="${sessionScope.acc.getAccount_role() == 2}">
                                        <a href="managehouse" style="font-size: 12px;"><i class="fa fa-power-off"style="color: #1e88e5"></i>Manage house</a>
                                    </c:if>
                                </div>
                            </div>
                        </c:if>
                        <c:if test="${sessionScope.acc == null}">
                            <li><a href="login.jsp"><i class="fa fa-user-o"></i> Login</a></li>
                            </c:if>
                    </ul>
                </div>
            </div>
            <!-- /TOP HEADER -->

            <!-- MAIN HEADER -->
            <div id="header">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- LOGO -->
                        <div class="col-md-3">
                            <div class="header-logo">
                                <a href="home.jsp" class="logo">
                                    <img src="usercss/images/CourseImage/house.png" alt=""/>

                                </a>
                            </div>
                        </div>
                        <!-- /LOGO -->

                        <!-- SEARCH BAR -->
                        <div class="col-md-6">
                            <div class="header-search">
                                <form action="searchcourse" method="get">
                                    <select class="input-select" name="filtersearch" required>
                                        <c:if test="${not empty filtersearch }" >
                                            <option value="${filtersearch}" selected hidden>${filtersearch}</option>
                                        </c:if>
                                        <option value="All"> All </option>
                                        <option value="Category Name"> Category Name </option>
                                        <option value="House Name"> House Name </option>
                                    </select>
                                    <input class="input" placeholder="Search here" name="search" type="text" value="${search}">
                                    <input type="hidden" name="modeManage" value="1">
                                    <button class="search-btn" type="submit">Search</button>
                                </form>
                            </div>
                        </div>
                        <!-- /SEARCH BAR -->

                        <!-- ACCOUNT -->
                        <div class="col-md-3 clearfix">
                            <div class="header-ctn">
                                <!-- Wishlist -->

                                <!-- /Wishlist -->

                                <!-- Cart -->
                                <div class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                        <i class="fa fa-shopping-cart"></i>
                                        <span>Your Cart</span>
                                        <div class="qty">
                                            <c:out value="${sessionScope.acc == null || sessionScope.size eq  0 ? '0' : sessionScope.size}" />
                                        </div>
                                    </a>
                                    <div class="cart-dropdown">
                                        <div class="cart-list">
                                            <c:forEach items="${listS}" var="o">
                                                <div class="product-widget">
                                                    <div class="product-img">
                                                        <img src="${o.image}" alt="">
                                                    </div>
                                                    <div class="product-body">
                                                        <p class="product-category">${o.categories.describe}</p>
                                                        <h3 class="product-name"><a href="detail?pid=${o.productID}">${o.productName}</a></h3>
                                                        <h4 class="product-price">${o.price}</h4>
                                                    </div>
                                                </div>

                                            </c:forEach>
                                        </div>
                                        <div class="cart-summary">
                                            <small>${sessionScope.size} items selected</small>
                                            <fmt:formatNumber var="totalMoney" value="${sessionScope.cart.getTotalMoney()}" type="currency" currencySymbol="" />
                                            <h5 class="price">Total: ${totalMoney} VNĐ</h5>
                                        </div>
                                        <div class="cart-btns">
                                            <a href="cart-list.jsp">View Cart</a>
                                        </div>
                                    </div>
                                </div>                         
                                <!-- /Cart -->

                                <!-- Menu Toogle -->
                                <div class="menu-toggle">
                                    <a href="#">
                                        <i class="fa fa-bars"></i>
                                        <span>Menu</span>
                                    </a>
                                </div>
                                <!-- /Menu Toogle -->
                            </div>
                        </div>
                        <!-- /ACCOUNT -->
                    </div>
                    <!-- row -->
                </div>
                <!-- container -->
            </div>
            <script>
                var priceElements = document.querySelectorAll('.price');
                priceElements.forEach(function (element) {
                    var priceText = element.textContent;

// Kiểm tra xem giá trị có chứa chuỗi ".00" hay không
                    if (priceText.includes(".00")) {
// Loại bỏ chuỗi ".00" bằng cách sử dụng hàm replace()
                        var formattedPrice = priceText.replace(".00", "");

// Gán lại giá trị đã được khử đuôi ".00"
                        element.textContent = formattedPrice;
                    }
                });
            </script>
            <!-- /MAIN HEADER -->
        </header>
        <!-- /HEADER -->

        <c:if test="${msg ne null}">
            <script>
                alert('${msg}');
            </script>
        </c:if>


        <!-- BREADCRUMB -->
        <div id="breadcrumb" class="section">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-md-12">
                        <ul class="breadcrumb-tree">
                            <li><a href="home">Home</a></li>
                            <li><a href="managehouse">Manage House</a></li>
                        </ul>
                    </div>
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /BREADCRUMB -->

        <!-- SECTION -->
        <div class="section">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- ASIDE -->
                    <div id="aside" class="col-md-3">
                        <!-- aside Widget -->
                        <form  id="myFilterCourse" action="filtermanagehouse" method="get">
                            <div class="aside">
                                <h3 class="aside-title">Categories</h3>
                                <div class="checkbox-filter">
                                    <c:forEach items="${listCategories}" var="i">
                                        <div class="input-checkbox">
                                            <input type="checkbox" ${checkedCategories.contains(i.getCategory_id()) ? 'checked' : ''} id="${i.getCategory_id()}" name="category_id" value="${i.getCategory_id()}" onchange="submitForm()" />
                                            <label for="${i.getCategory_id()}">
                                                <span></span>
                                                ${i.getCategory_name()}
                                            </label>
                                        </div>
                                    </c:forEach>
                                </div>
                            </div>
                            <!-- /aside Widget -->

                            <!-- aside Widget -->
                            <div class="aside">
                                <h3 class="aside-title">Price</h3>
                                <div class="store-sort">

                                    <select class="input-select" name="sortHouse" onchange="submitForm()">
                                        <option ${sortCourse == 0 ? "selected":""} value="0">Price Max to Min</option>
                                        <option ${sortCourse == 1 ? "selected":""} value="1">Price Min to Max</option>
                                    </select> 


                                </div>
                            </div>
                        </form>
                        <script>
                            function submitForm() {
                                document.getElementById("myFilterCourse").submit(); // Gửi yêu cầu khi có thay đổi
                            }
                        </script>

                        <!-- aside Widget -->
                        <div class="aside">
                            <h3 class="aside-title">Top selling</h3>

                        </div>
                        <!-- /aside Widget -->
                    </div>
                    <!-- /ASIDE -->

                    <!-- STORE -->
                    <div id="store" class="col-md-9">
                        <!-- store top filter -->
                        <div class="store-filter clearfix">

                            <!--                            <ul class="store-grid">
                                                            <li class="active"><i class="fa fa-th"></i></li>
                                                            <li><a href="#"><i class="fa fa-th-list"></i></a></li>
                                                        </ul>-->
                        </div>

                        <!-- /store top filter -->

                        <!-- store products -->
                        <div class="row">
                            <!-- product -->
                            <div style="width: 100%; text-align: right">
                                <input id="add" style="color: white; background-color: #1e88e5; padding: 10px 20px; border-radius: 20px; border: none" type="button" name="name" value="Add New" onclick="add()">
                                <a style="color: white; background-color: #1e88e5; padding: 10px 20px; border-radius: 20px; border: none" href="viewOrder">View order</a>
                            </div>
                            <c:forEach items="${listHouseOfCateByPaging}" var="i">
                                <div class="col-md-4 col-xs-6" >
                                    <div class="product">
                                        <div class="product-img">
                                            <img src="${i.img1}">
                                            <div class="product-label">
                                                <span class="sale">${i.getDiscount()}%</span>
                                                <span class="new">NEW</span>
                                            </div>
                                        </div>
                                        <div class="product-body" style="width: 262.5px; height: 133.4px">
                                            <p class="product-category">${i.getCategories().getCategory_name()}</p>
                                            <h3 class="product-name"><a href="detail?course_id=${i.getId()}">${i.getName()}</a></h3>
                                                <fmt:formatNumber var="productprice" value="${i.getPrice()*(100-i.getDiscount())/100}" type="currency" currencySymbol="₫" />

                                            <h4 class="product-price">${productprice}
                                                <fmt:formatNumber var="productoldprice" value="${i.getPrice()}" type="currency" currencySymbol="₫" />
                                                <del class="product-old-price">${productoldprice}</del></h4>

                                        </div>
                                        <div class="add-to-cart">
                                            <form action="crudhouse" onsubmit="return confirmDelete();">
                                                <input type="hidden" name="id" value="${i.getId()}" />
                                                <input type="hidden" name="mode" value="delete" />
                                                <button type="submit" class="add-to-cart-btn"><i class="fa fa-trash"></i>Delete</button>
                                            </form>
                                            <script>
                                                function confirmDelete() {
                                                    return confirm("Are you sure you want to delete?");
                                                }
                                            </script>


                                            <form action="crudhouse" style="margin-top: 20px">
                                                <input type="hidden" name="id" value="${i.getId()}" />
                                                <input type="hidden" name="mode" value="update"/>
                                                <button type="submit" class="add-to-cart-btn"><i class="fa fa-pencil"></i>View Details</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>

                            <!-- /product -->

                            <!-- /product -->
                        </div>
                        <!-- /store products -->

                        <!-- store bottom filter -->

                        <div class="store-filter clearfix">
                            <ul class="store-pagination">
                                <c:if test="${endP != 1}">
                                    <c:forEach begin="1" end="${endP}" var="i">
                                        <c:if test="${mode == 'houseofcategory'}">
                                            <li><a class="${index == i? "active":""}" href="managehouse?index=${i}">${i}</a></li>
                                            </c:if>
                                            <c:if test="${mode == 'filter'}">
                                                <c:if test="${selectedCategories != null}">
                                                <li><a class="${index == i ? 'active' : ''}" href="filtermanagehouse?sortCourse=${sortCourse}&index=${i}${selectedCategories}">${i}</a></li>
                                                </c:if>
                                                <c:if test="${selectedCategories == null}">
                                                <li><a class="${index == i ? 'active' : ''}" href="filtermanagehouse?sortCourse=${sortCourse}&index=${i}">${i}</a></li>
                                                </c:if>
                                            </c:if>
                                            <c:if test="${mode == 'searchcourse'}">
                                            <li><a class="${index == i? "active":""}" href="searchcourse?filtersearch=${filtersearch}&search=${search}&index=${i}">${i}</a></li>
                                            </c:if>
                                        </c:forEach>
                                    </c:if>
                            </ul>
                        </div>
                        <!-- /store bottom filter -->
                    </div>
                    <!-- /STORE -->
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /SECTION -->

        <!-- NEWSLETTER -->
        <div id="newsletter" class="section">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="newsletter">
                            <p>Sign Up for the <strong>NEWSLETTER</strong></p>
                            <form>
                                <input class="input" type="email" placeholder="Enter Your Email">
                                <button class="newsletter-btn"><i class="fa fa-envelope"></i> Subscribe</button>
                            </form>
                            <ul class="newsletter-follow">
                                <li>
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-pinterest"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /NEWSLETTER -->

        <jsp:include page="footer.jsp"></jsp:include>

        <!-- jQuery Plugins -->
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/slick.min.js"></script>
        <script src="js/nouislider.min.js"></script>
        <script src="js/jquery.zoom.min.js"></script>
        <script src="js/main.js"></script>
        <script>
                                                function add() {
                                                    window.location.href = './crudhouse?mode=add';
                                                }

        </script>
    </body>
</html>


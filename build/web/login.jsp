<%-- 
    Document   : login
    Created on : Mar 11, 2023, 7:51:36 PM
    Author     : acer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html lang="en" dir="ltr">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="login/css/style.css">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            .loginGoogle{
                background-color: rgba(51, 51, 51, 0.05);
                border-radius: 8px;
                border-width: 0;
                color: #333333;
                cursor: pointer;
                display: inline-block;
                font-family: "Haas Grot Text R Web", "Helvetica Neue", Helvetica, Arial, sans-serif;
                font-size: 14px;
                font-weight: 500;
                line-height: 20px;
                list-style: none;
                margin: 0;
                padding: 10px 12px;
                text-align: center;
                transition: all 200ms;
                vertical-align: baseline;
                white-space: nowrap;
                user-select: none;
                -webkit-user-select: none;
                touch-action: manipulation;
            }
        </style>

        <script src="https://www.gstatic.com/firebasejs/7.15.0/firebase-app.js"></script>

        <!-- Add additional services you want to use -->
        <script src="https://www.gstatic.com/firebasejs/7.15.0/firebase-auth.js"></script>

        <script>
            // Your web app's Firebase configuration
            const firebaseConfig = {
                apiKey: "AIzaSyB9z0KvbNijNljmrvcMF6wcUAgy5OdoKVY",
                authDomain: "test-4617f.firebaseapp.com",
                projectId: "test-4617f",
                storageBucket: "test-4617f.appspot.com",
                messagingSenderId: "1023853918396",
                appId: "1:1023853918396:web:f09e2291a82517609638b9"
            };

            // Initialize Firebase
            firebase.initializeApp(firebaseConfig);
        </script>
    </head>
    <body>
        <div class="container">
            <input type="checkbox" id="flip">
            <div class="cover">
                <div class="front">
                    <img src="./usercss/images/CourseImage/loginhomesharing.png" alt="">
                    <div class="text">
                        <span class="text-1"></span>
                        <span class="text-2"></span>
                    </div>
                </div>
                <div class="back">
                    <img class="backImg" src="img/logomarket.png" alt="">
                    <div class="text">
                        <span class="text-1">Complete miles of journey <br> with one step</span>
                        <span class="text-2">Let's get started</span>
                    </div>
                </div>
            </div>
            <div class="forms">
                <div class="form-content">
                    <div class="login-form">
                        <div class="title">Login</div>
                        <form action="login" method="post">                                
                            <div class="input-boxes">
                                <div class="input-box">
                                    <i class="fas fa-envelope"></i>               
                                    <input type="text" placeholder="Enter your username " name="username">
                                </div>
                                <div class="input-box">
                                    <i class="fas fa-lock"></i>
                                    <input type="password" placeholder="Enter your password" name="password">
                                </div>
                                <div class="text"><a href="forgotPassword.jsp">Forgot password?</a></div>
                                <p class="alert-danger" style="color: red">
                                    ${mess}
                                </p>
                                <p class="alert-danger" style="color: red">
                                    ${smess}
                                </p>
                                <div class="button input-box">
                                    <input type="submit" value="Login">
                                    <p class="loginGoogle" onclick="loginWithGoogle()">Login google</p>
                                </div>
                                <div class="text sign-up-text">Don't have an account? <a href="signup.jsp">Signup?</a></div>
                            </div>
                        </form>
                    </div>        
                </div>
            </div>
        </div>

        <form action="loginGoogle" method="post" class="loginGoogleForm" style="display: none;">
            <input type="hidden" name="email" value="">
        </form>

        <script>
            const provider = new firebase.auth.GoogleAuthProvider();

            function loginWithGoogle() {
                //dich vu firebase
                //ho tro xac minh chinh chu tai khoan mail
                firebase.auth().signInWithPopup(provider).then(function (result) {
                    // This gives you a Google Access Token. You can use it to access the Google API.
                    let token = result.credential.accessToken;
                    // The signed-in user info.
                    let user = result.user;

                    //lay form
                    let form = document.querySelector('.loginGoogleForm');
                    //gan mail vao form
                    let input = form.querySelector('input');
                    input.value = user.email;
                    form.submit();
                });
            }
        </script>

    </body>
</html>
